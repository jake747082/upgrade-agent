source 'http://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.7'

# Use mysql as the database for Active Record
gem 'mysql2', '~> 0.3.18'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.2'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.1'

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.1.2'
gem 'oj'

gem 'thor', '0.19.1'

# Style, Views, Template (libraries & tools)
gem 'bootstrap-sass-rails'
gem 'bootstrap-datepicker-rails'
gem 'will_paginate'
gem "bootstrap_helper", git: 'https://github.com/afunction/bootstrap-helper.git'
gem 'font-awesome-rails'
gem 'http_accept_language'
gem 'rails_autolink', '~> 1.1.6'

gem 'rails4-autocomplete'
gem 'jquery-ui-sass-rails'

gem 'cells', '3.11.3'
gem 'haml-rails'
gem 'nprogress-rails'
gem 'zeroclipboard-rails'

# Form Objects
gem 'reform'
gem 'virtus'

# SEO & Analytics
gem 'seo_helper', '~> 1.0.2'

# Forms & Helpers
gem "bootstrap_form"

# Export Excel
gem 'axlsx_rails'

# Models
gem 'super_accessors'
gem 'validates_email_format_of'
gem 'ipaddress'
gem 'dalli'
gem 'simple_enum'
gem 'soft_deletion'
gem 'awesome_nested_set'
gem 'stamp', '0.4.0' # does not work with newer versions
gem 'stamp-i18n'
gem 'public_activity', '1.5.0'

# User System
gem 'devise'

# Others tools
gem 'aescrypt'
gem 'rails-i18n'
gem 'settingslogic'
gem 'actionpack-action_caching'
gem 'actionpack-page_caching'

# Server Solutions
gem 'puma', '2.11.1'
gem 'pusher'
gem 'memoist'

# ACL
gem 'pundit', '0.3.0'

# papertrail
gem 'remote_syslog_logger'

gem 'exception_notification', git: 'https://github.com/smartinez87/exception_notification.git'
gem 'slack-notifier'

# 所有環境都綁，包含 production
gem "pry-rails"
gem "awesome_print", require: false

gem 'gambo', git: 'git@gitlab.com:btsi365.corp/bt_lobby/gambo.git', branch: 'develop'
# gem 'gambo', path: '../gambo'

gem 'visionbundles', git: 'git@gitlab.com:btsi365.corp/visionbundles.git', branch: 'master'

gem 'faker', '1.9.1'

gem 'rake', '12.3.3'
gem 'minitest', '5.11.3'

gem 'dry-inflector', '0.1.2'

gem 'ar-octopus' #db讀寫分離

gem 'rqrcode', '0.10.1'

# SMS
gem 'sms_mitake', git: 'git@gitlab.com:btsi365.corp/gems/sms_mitake.git', branch: 'master'

group :production do
  gem 'newrelic_rpm'
end

group :development, :test do # 也包含 test 是為了讓寫 test case 時也可以 debug
  gem "pry-plus"
  gem "hirb", require: false
  gem "hirb-unicode", require: false
  gem 'simplecov', require: false
  gem "pry-remote"
  gem 'capybara'
  gem 'poltergeist'
  gem 'parallel_tests'
end

group :development do
  gem 'capistrano', '~> 2.15.5', require: false
  gem 'rvm-capistrano'
  gem 'binding_of_caller'
  gem 'better_errors'
  gem 'powder'
  gem 'database_cleaner'
  gem 'rspec-rails', '~> 3.0.0'
  gem 'colorize'
end
