namespace :fake do
  desc "Rebuild fake betform"
  task :build => ["tmp:clear", "log:clear", "db:drop", "db:create", "db:migrate", "fake:rebuild", "fake:build_admin" ]

  task :rebuild => :environment do
    build_agents_and_users(3)
    build_bet_form(500)
  end

  task :build_admin => :environment do
    # 管理員帳號
    Admin.create!(username: 'system', nickname: '米邦 (系統管理員)', password: 'password', password_confirmation: 'password', role: :super_manager)
    Admin.create!(username: 'system_customer', nickname: '米邦 (客服)', password: 'password', password_confirmation: 'password', role: :custom_service)
    Admin.create!(username: 'system_report', nickname: '米邦 (報表)', password: 'password', password_confirmation: 'password', role: :report_manager)
  end

  task :build_bet_forms => :environment do
    build_bet_form(500)
  end

  task :build_users => :environment do
    build_agents_and_users(3)
  end


  def build_agents_and_users(shareholder_count=3)
    director_count = shareholder_count * 3
    agent_count = director_count * 3
    sub_agent_count = agent_count * 3
    user_count = sub_agent_count * 3
    password = 'password'
    begin
      (1..shareholder_count).each do |shareholder_c|
        shareholder = Agent.new(username: "rand_shareholder_#{shareholder_c}",
                                nickname: "亂數股東#{shareholder_c}",
                                password: password, password_confirmation: password,
                                video_game_ratio: rand(0..10) + 90, casino_game_ratio: rand(0..10) + 90)
        AgentSaver.new(shareholder).save!
      end

      (1..director_count).each do |director_c|
        director = Agent.new(username: "rand_director_#{director_c}",
                                nickname: "亂數總監#{director_c}",
                                password: password, password_confirmation: password,
                                parent: Agent.all_shareholder.reorder('rand()').first,
                                video_game_ratio: rand(0..10) + 80, casino_game_ratio: rand(0..10) + 80)
        AgentSaver.new(director).save!
      end

      (1..agent_count).each do |agent_c|
        agent = Agent.new(username: "rand_agent_#{agent_c}",
                                nickname: "亂數總代#{agent_c}",
                                password: password, password_confirmation: password,
                                parent: Agent.all_director.reorder('rand()').first,
                                video_game_ratio: rand(0..20) + 60, casino_game_ratio: rand(0..20) + 60)
        AgentSaver.new(agent).save!
      end

      (1..sub_agent_count).each do |sub_agent_c|
        sub_agent = Agent.new(username: "rand_sub_agent_#{sub_agent_c}",
                                nickname: "亂數代理#{sub_agent_c}",
                                password: password, password_confirmation: password,
                                parent: Agent.all_agent.reorder('rand()').first,
                                video_game_ratio: rand(0..20) + 40, casino_game_ratio: rand(0..20) + 40)
        AgentSaver.new(sub_agent).save!
      end
      (1..user_count).each do |user_c|
        Agent.all_sub_agent.reorder('rand()').first.users.create!(
            username: "rand_player_#{user_c}",
            nickname: "亂數玩家#{user_c}",
            password: password, password_confirmation: password).save!
      end
    rescue
    end
  end

  def build_bet_form(count=500)
    (1..count).each do |j|
      game_id = [:baccarat, :slot].sample

      amount = Random.rand(200) * 10 + 500
      user_win = if Random.rand(10) > 3
        amount * (Random.rand(20) + 80) / 100
      else
        0
      end

      bet_logger = BetLogger.new(
        User.reorder('rand()').first,
        game_id, amount, user_win)
      bet_logger.save
    end
  end
end
