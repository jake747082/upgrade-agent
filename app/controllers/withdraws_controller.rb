# 玩家提款處理
class WithdrawsController < ApplicationController
  before_action :set_withdraw, except: :index
  before_action :set_breadcrumbs_path, :valid_permission!

  def index
    if params[:status].nil? || params[:status] == 'all'
      @withdraws = current_agent.withdraws.list_by_agent.search(params[:query]).page(params[:page])
    else
      @withdraws = current_agent.withdraws.list_by_agent.send("list_#{params[:status]}").search(params[:query]).page(params[:page])
    end
    @withdarw_count = current_agent.withdraws.list_by_agent.search(params[:query]).group(:status).count
  end

  # PUT /withdarws/1
  def update
    state = if params[:status] == 'transferred'
      # @withdraw.user.create_log("cashout", current_account, remote_ip ,credit: @withdraw.credit_actual, withdraw_id: @withdraw.id)
      @withdraw.finish!
      send_message(title: I18n.t("messages.withdraw.transferred.title"), content: I18n.t("messages.withdraw.transferred.content_html", link: "/account/withdraws/#{@withdraw.order_id}", order_id: @withdraw.order_id, credit: @withdraw.credit_actual), username: @withdraw.user.username, groups: [])
    elsif params[:status] == 'proccessing'
      @withdraw.proccess!
      send_message(title: I18n.t("messages.withdraw.proccessing.title"), content: I18n.t("messages.withdraw.proccessing.content_html", link: "/account/withdraws/#{@withdraw.order_id}", order_id: @withdraw.order_id, credit: @withdraw.credit), username: @withdraw.user.username, groups: [])
    else
      false
    end
    redirect_url(state, params[:status])
  end

  def destroy
    state = if params[:status] == 'removed'
      @withdraw.remove!
      send_message(title: I18n.t("messages.withdraw.removed.title"), content: I18n.t("messages.withdraw.removed.content_html", link: "/account/withdraws/#{@withdraw.order_id}", order_id: @withdraw.order_id, credit: @withdraw.credit), username: @withdraw.user.username, groups: [])
    elsif params[:status] == 'rejected'
      # @TODO 目前沒用到這個功能
      @withdraw.reject!
    else
      false
    end
    redirect_url(state, params[:status])
  end

  def show; end

  protected

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.withdraws.handle_cashout'), withdraws_path)
  end

  def set_withdraw
    @withdraw = current_agent.withdraws.list_by_agent.find(params[:id])
  end

  def valid_permission!
    authorize :withdraw, :all?
  end

  def redirect_url(state, status)
    # save log
    @withdraw.user.create_log('cashout', current_account, remote_ip, status: status, credit: @withdraw.credit_actual.to_f, id: @withdraw.trade_no)

    if state
      redirect_to withdraws_url(status: status), notice: t("flash.withdraw.#{status}")
    else
      redirect_to withdraws_url(status: status), alert: t("flash.withdraw.#{status}_faild")
    end
  end
end
