class Histories::ElectronicGameController < Histories::BaseController
  include ApplicationHelper

  private

  def set_breadcrumb
    drop_breadcrumb(t('breadcrumbs.histories.electronic_game'))
  end

  def set_bet_forms
    @bet_forms = current_agent.all_electronic_bet_forms.where(role_search_params).date_range(@begin_date, @end_date).includes(:user, :agent, :director, :shareholder)
  end

  def role_search_params
    params[:role].present? ? params.require(:role).permit(:agent_id, :user_id, :shareholder_id, :director_id) : {}
  end
end