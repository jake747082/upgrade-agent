class Histories::RacingController < Histories::BaseController

  # GET histories/racings/1.js
  def show
    respond_to do |format|
      format.js {
        @racing_bet_form = current_agent.all_racing_bet_forms.find(params[:id])
        @sub_bet_forms = @racing_bet_form.racing_sub_bet_forms
      }
    end
  end

  private

  def set_breadcrumb
    drop_breadcrumb(t('breadcrumbs.histories.racing'))
  end

  def set_bet_forms
    @bet_forms = current_agent.all_racing_bet_forms.where(role_search_params).date_range(@begin_date, @end_date).includes(:user, :agent, :director, :shareholder)
  end

  def role_search_params
    if params[:role].present?
      params.require(:role).permit(:agent_id, :machine_id, :shareholder_id, :director_id, :racing_schedule_id)
    else
      {}
    end
  end
end
