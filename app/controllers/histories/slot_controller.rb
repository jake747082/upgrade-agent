class Histories::SlotController < Histories::BaseController

  def index
    data = {type: params[:type], begin_date: params[:begin_date], end_date: params[:end_date], role: params[:role]}
    redirect_to histories_game_index_url(data)
  end

  private

  def set_breadcrumb
    drop_breadcrumb(t('breadcrumbs.histories.slot'))
  end

  def set_bet_forms
    @bet_forms = current_agent.all_slot_bet_forms.where(role_search_params).date_range(@begin_date, @end_date).includes(:user, :agent, :director, :shareholder)
  end

  def role_search_params
    if params[:role].present?
      params.require(:role).permit(:agent_id, :machine_id, :shareholder_id, :director_id, :user_id)
    else
      {}
    end
  end
end
