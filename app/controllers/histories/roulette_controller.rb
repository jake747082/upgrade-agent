class Histories::RouletteController < Histories::BaseController
  before_action :valid_not_shop_site!

  private

  def set_breadcrumb
    drop_breadcrumb(t('breadcrumbs.histories.roulette'))
  end

  def set_bet_forms
    @bet_forms = current_agent.all_roulette_bet_forms.where(role_search_params).date_range(@begin_date, @end_date).includes(:user, :agent, :director, :shareholder)
  end

  def role_search_params
    if params[:role].present?
      params.require(:role).permit(:agent_id, :shareholder_id, :director_id, :user_id)
    else
      {}
    end
  end
end
