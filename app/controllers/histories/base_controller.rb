class Histories::BaseController < ApplicationController
  include DateRangeFilter
  before_action :valid_permission!, :set_agent_breadcrumbs_path, :set_default_date_range, :set_bet_forms
  before_action :set_breadcrumb, only: :index

  def index
    @bet_forms = @bet_forms.page(params[:page])
    @role_search_params = role_search_params
  end

  def export
    respond_to do |format|
      format.xlsx {
        render xlsx: "export", filename: "#{Setting.title}-#{params[:controller]}-(#{@begin_date}_to_#{@end_date}).xlsx"
      }
    end
  end

  protected

  def set_agent_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.history'), histories_game_index_path)
  end

  def valid_permission!
    authorize :history, :all?
  end

  def valid_not_shop_site!
    render_404! if Setting.shop_site?
  end

  def valid_cash_site!
    render_404! unless Setting.cash_site?
  end
end