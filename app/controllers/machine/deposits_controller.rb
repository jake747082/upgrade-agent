class Machine::DepositsController < Machine::BaseController
  before_action :valid_permission!, :authorize_unlocked_agent!
  before_action :set_machine, :set_machine_form

  # GET /machines/1/depisits/new
  def new
    respond_to do |format|
      format.js
    end
  end
  # POST /machines/1/depisits
  def create
    respond_to do |format|
      format.js { 
        @status = @deposit_form.deposit

        if @status
          @machine_log = @machine.create_log(:deposit, current_account, remote_ip, credit: @deposit_form.deposit_credit)
          # 開機：擺這裡 deposit + 儲值時才會連 credit 一起過去
          if @machine.shutdown?
            Machine::Control.new(@machine).boot!
          else
            Machine::Control.new(@machine).deposit!(@deposit_form.deposit_credit)
          end
        end
      }
    end
  end
  
  private
    def set_machine_form
      @deposit_form = MachineForm::Deposit.new(@machine, params[:deposit] || { deposit_credit: 0 })
    end
end