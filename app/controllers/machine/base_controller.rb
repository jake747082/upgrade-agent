class Machine::BaseController < ApplicationController
  before_action :set_breadcrumbs_path

  private

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.machine.root'), machines_path)
  end

  def valid_permission!
    authorize :common, :machines_control?
  end

  def set_machine
    @machine = current_agent.all_machines.find(params[:machine_id])
  end
end
