class Machine::ControlsController < Machine::BaseController
  before_action :valid_permission!, :authorize_unlocked_agent!
  before_action :set_machine

  # 開機
  # POST /machines/1/controls
  def create
    respond_to do |format|
      format.js { 
        Machine::Control.new(@machine).boot! do |machine|
          machine.create_log(:boot, current_account, remote_ip)
        end
      }
    end
  end

  # 關機
  # DELETE /machines/1/controls
  def destroy
    respond_to do |format|
      @cashout_form = MachineForm::Cashout.new(@machine) 
      format.js {
        Machine::Control.new(@machine).shutdown! do |machine|
          machine.create_log(:shutdown, current_account, remote_ip)
          if machine.credit_left == 0
            @machine.update!(credit_max: 0, credit_used: 0)
            @machine_log = machine.create_log(:cashout, current_account, remote_ip, credit: 0)
          end
        end
      }
    end
  end
end