class Machine::CashoutsController < Machine::BaseController
  before_action :authorize_unlocked_agent!, only: [:withdraw]
  before_action :set_machine, :valid_permission!
  before_action :set_cashout_view, only: [:new, :withdraw]

  # GET /machines/1/cashouts/new
  def new; end

  # POST /machines/1/cashouts
  def create
    respond_to do |format|
      format.js{
        @cashout_form = MachineForm::Cashout.new(@machine, params[:cashout] || { cashout_credit: 0 })
        if @status = @cashout_form.cashout
          @machine_log = @machine.create_log(:cashout, current_account, remote_ip, credit: @cashout_form.cashout_credit)
          Machine::Control.new(@machine).cashout!(@cashout_form.cashout_credit)
        end
      }
    end
  end

  def withdraw; end

  private

  def set_cashout_view
    @cashout_form = MachineForm::Cashout.new(@machine)
    respond_to do |format|
      format.js
    end
  end
end