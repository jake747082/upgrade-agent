class Machine::MaintainsController < Machine::BaseController
  before_action :valid_permission!, :authorize_unlocked_agent!
  before_action :set_agent_auth_form , only: [:new, :create]

  # GET /maintains/new
  def new; end

  # POST /maintains
  def create
    params[:auth] ||= {}
    unless @auth_form.verify(params[:auth][:password]) { current_agent.generate_maintain_code }
      render :new
    end
  end

  # DELETE /maintains
  def destroy
    current_agent.update!(maintain_code: '')
    redirect_to new_maintains_url, notice: t('flash.machine.close_maintain.finish')
  end

  private

  def set_agent_auth_form
    drop_breadcrumb(t('breadcrumbs.maintain'))
    @auth_form = AgentForm::Auth.new(current_account.username)
  end

  def valid_permission!
    super
    authorize :maintain, :all?
  end
end