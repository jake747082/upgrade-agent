class Machine::LogsController < Machine::BaseController
  before_action :valid_permission!, :set_machine

  # GET /machines/1/logs
  def index
    drop_breadcrumb(@machine.title)
    @logs = @machine.change_logs.where.not(owner_type: 'Admin').page(params[:page])
    render 'logs/index'
  end

  protected

  def valid_permission!
    authorize :common, :machines_list?
  end
end