class Account::BaseController < ApplicationController
  before_action :set_account_breadcrumbs_path, :valid_permission!

  private

  def set_account_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.sub_accounts.root'), accounts_path)
  end

  def set_account
    @account = current_agent.accounts.find(params[:account_id] || params[:id])
  end

  def valid_permission!
    authorize :common, :sub_accounts?
  end
end
