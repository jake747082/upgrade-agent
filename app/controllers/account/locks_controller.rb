class Account::LocksController < Account::BaseController
  before_action :authorize_unlocked_agent!, :set_account

  # POST /accounts/:account_id/locks
  def create
    @account.update!(lock: true)
    @account.create_log(:lock, current_account, remote_ip)
    redirect_to edit_account_path(@account), notice: t('flash.account.lock', account: @account.system_id)
  end

  # DELETE /accounts/:account_id/locks
  def destroy
    @account.update!(lock: false)
    @account.create_log(:unlock, current_account, remote_ip)
    redirect_to edit_account_path(@account), notice: t('flash.account.unlock', account: @account.system_id)
  end
end