class Account::LogsController < Account::BaseController
  before_action :set_account, :set_breadcrumbs_path

  # GET /agents/1/logs
  def index
    @logs = @account.action_logs.page(params[:page])
  end

  # GET /agents/1/logs/change
  def change
    @logs = @account.change_logs.page(params[:page])
    render :index
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(@account.system_id)
  end
end