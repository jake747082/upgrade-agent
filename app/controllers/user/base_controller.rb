class User::BaseController < ApplicationController
  before_action :set_user_breadcrumbs_path

  protected

  def set_user_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.user.root'), users_path)
  end

  def set_user
    @user = User.find(params[:id] || params[:user_id])
  end
end