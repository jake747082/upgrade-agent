class User::DetailsController < User::BaseController
  before_action :set_user

  def index
    respond_to do |format|
      format.js
    end
  end
end