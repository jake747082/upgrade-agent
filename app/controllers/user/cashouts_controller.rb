class User::CashoutsController < User::BaseController
  before_action :valid_permission!
  before_action :set_user

  # GET /users/1/cashouts/new
  def new
    respond_to do |format|
      format.js {
        if has_agent_permission?
          @cashout_form = UserForm::Cashout.new(@user, { cashout_credit: 0 })
        else
          # 無權限
          @cashout_form = false
        end
      }
    end
  end
  # POST /users/1/cashouts
  def create
    respond_to do |format|
      format.js {
        if has_agent_permission?
          service = User::Points::CrossLevelsCashout.new(current_agent, @user, cashout_params[:cashout_credit].to_f)
          @status = service.cashout!
          @cashout_form = service.user_form
          if @status
            send_message(title: I18n.t("messages.cashouts.title"), content: I18n.t("messages.cashouts.content", credit: @cashout_form.cashout_credit), username: @user.username, groups: [])
            @user_log = @user.create_log(:cashout, current_agent, remote_ip, credit: @cashout_form.cashout_credit, withdraw_id: @cashout_form.withdraw.id, trade_no: @cashout_form.withdraw.trade_no, status: 'transferred')
          else
            @cashout_form = UserForm::Cashout.new(@user, { cashout_credit: 0 })
            @errors = service.errors
          end
          @user.reload # reload user info
        end
      }
    end
  end

  private

    # 上下分權限
    # 1. 代理最後一層
    # 2. 登入代理是該玩家代理的擁有者
    def has_agent_permission?
      (current_agent.in_top_level? && current_agent.id == @user.agent.owner_id) || current_agent.in_final_level?
    end

    def valid_permission!
      authorize :user, :update?
    end

    def cashout_params
      params.require(:cashout).permit(:cashout_credit)
    end
end