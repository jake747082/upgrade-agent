class User::DepositsController < User::BaseController
  before_action :set_user
  before_action :valid_permission!

  # GET /users/1/depisits/new
  def new
    respond_to do |format|
      format.js {
        if has_agent_permission?
          @deposit_form = UserForm::Deposit.new(@user, { deposit_credit: 0 })
        else
          # 無權限
          @deposit_form = false
        end
      }
    end
  end
  # POST /users/1/depisits
  def create
    respond_to do |format|
      format.js {
        if has_agent_permission?
          service = User::Points::CrossLevelsDeposit.new(current_agent, @user, deposit_params[:deposit_credit].to_f)
          @status = service.deposit!
          @deposit_form = service.user_form
          if @status
            send_message(title: I18n.t("messages.deposits.title"), content: I18n.t("messages.deposits.content", credit: @deposit_form.deposit_credit), username: @user.username, groups: [])
            @user_log = @user.create_log(:deposit, current_agent, remote_ip, credit: @deposit_form.deposit_credit, withdraw_id: @deposit_form.withdraw.id, trade_no: @deposit_form.withdraw.trade_no, status: 'transferred')
          else
            @deposit_form = UserForm::Deposit.new(@user, { deposit_credit: 0 }) if @deposit_form.nil?
            @errors = service.errors
          end
          set_user # reload user info
        end
      }
    end
  end

  private

    # 上下分權限
    # 1. 代理最後一層
    # 2. 登入代理是該玩家代理的擁有者
    def has_agent_permission?
      (current_agent.in_top_level? && current_agent.id == @user.agent.owner_id) || current_agent.in_final_level?
    end

    def valid_permission!
      authorize :user, :update?
    end

    def deposit_params
      params.require(:deposit).permit(:deposit_credit)
    end
end