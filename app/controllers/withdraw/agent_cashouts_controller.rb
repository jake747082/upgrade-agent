class Withdraw::AgentCashoutsController < Withdraw::BaseController
  before_action :set_withdraw
  autocomplete :user, :username
  # GET /agent_withdraw/:cashouts/new
  def new; end

  def create
    if @withdraw.cashout
      @withdraw.agent.create_log(:cashout, current_agent, remote_ip ,points: @withdraw.points.to_f, agent_withdraw_id: @withdraw.model.id, trade_no: @withdraw.model.trade_no, status: 'transferred')
      unless @withdraw.agent.parent.nil?
        @withdraw.agent.parent.create_log(:refund, current_agent, remote_ip ,points: @withdraw.points.to_f, agent_withdraw_id: @withdraw.refund_model.id, trade_no: @withdraw.refund_model.trade_no, status: 'transferred', related_agent_withdraw_id: @withdraw.model.id)
      end
      redirect_to new_withdraw_agent_cashout_path, notice: t('flash.agent_withdraw.cashouts.finish')
    else
      render :new
    end
  end

  def autocomplete_agent_username
    term = params[:term]
    agents = current_agent.children.where('username LIKE ?', "%#{term}%").where(type_cd: 1).order(:username).all
    render :json => agents.map { |agent| {:id => agent.id, :label => agent.username, :value => agent.username} }
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t('breadcrumbs.agent_withdraws.cashouts'))
  end

  def set_withdraw
    @withdraw_params = params[:agent_withdraw] || {}
    @withdraw = AgentWithdrawForm::Cashout.new(@withdraw_params.merge(level_cd: current_agent.agent_level_cd + 1))
  end

  def withdraw_params
    params.require(:agent_withdraw).permit(:username, :points, :note)
  end

  def valid_permission!
    authorize :withdraw, :agent?
  end
end
