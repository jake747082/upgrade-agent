class Withdraw::BaseController < ApplicationController
  before_action :valid_permission!, :set_breadcrumbs

  protected

  def valid_permission!
    authorize :withdraw, :agent?
  end
end