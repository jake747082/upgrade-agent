class Withdraw::AgentWithdrawsController < Withdraw::BaseController
  before_action :set_withdraw_type

  def index
    agent = @agent_id.to_i == 0 ? current_agent : current_agent.descendants.find(params[:id])
    drop_breadcrumb(agent.system_id)
    @withdraws = agent.agent_withdraws.search(params[:query]).withdraw_type(params[:type]).page(params[:page])
  end
  
  def show
    @withdraw = current_agent.descendants.find(params[:agent_id]).agent_withdraws.includes(:agent).find(params[:id])
  end

  protected

  def set_breadcrumbs
    drop_breadcrumb(t("breadcrumbs.withdraws.agent_withdraws"), withdraw_agent_withdraws_path)
  end

  def set_withdraw_type
    @agent_id = params[:id]
    @type = params[:type]
  end

  def valid_permission!
    authorize :withdraws, :agent?
  end
end