class LogsController < Agent::BaseController

  # GET /logs
  def index
    @agent = current_agent
    @logs = if params[:query].present?
      @agent.user_change_logs(params[:query]).page(params[:page])
    else
      @agent.action_logs.page(params[:page])
    end
  end

  private

  def valid_permission!
    authorize :'agent/log'
  end
end