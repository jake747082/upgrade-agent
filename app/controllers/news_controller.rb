class NewsController < ApplicationController
  before_action :set_breadcrumbs_path, :valid_permission!

  def index
    @news = New.agent_list.page(params[:page])
  end

  def show
    @news = New.agent_list.find(params[:id])
  end

  protected

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.news'), news_index_path)
  end

  def valid_permission!
    authorize :news, :all?
  end
end
