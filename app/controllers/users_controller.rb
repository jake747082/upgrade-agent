class UsersController < User::BaseController
  include DateRangeFilter
  helper TransferCreditHelper
  
  before_action :valid_permission!
  before_action :authorize_unlocked_agent!, :set_user
  before_action :set_user, only: [:show, :edit, :update]
  before_action :set_default_date_range, :valid_date!, only: [:transfer_credit]

  # GET /users
  def index
    @users = current_agent.all_users.includes(:agent).search(params[:query]).page(params[:page])
  end

  # GET /users/1
  def show
    @agents = @user.agent.self_and_ancestors.where('agent_level_cd >= ?', current_agent.level)
  end

  # GET /users/1
  def edit
    @user_game_platformship = @user.user_game_platformships.east
  end

  # PUT/PATCH /users/1
  def update
    if @form.update(user_params)
      redirect_to @user, notice: t('flash.shared.update.finish', name: User.model_name.human)
    else
      render :edit
    end
  end

  # GET /users/limit_table
  def limit_table
    respond_to do |format|
      @limittype = params[:limittype].to_i == 1 ? 'video' : 'roulette'
      @east_limit = GamePlatforms::East.config['limit'][@limittype]
      format.js
    end
  end

def transfer_credit
    drop_breadcrumb(t('breadcrumbs.user.transfer_credit')+" "+User.find(params[:user_id]).title)
    set_transfer_credit_type
    @logs = PublicActivity::Activity.where(trackable: params[:user_id], trackable_type: User).where(updated_at: @begin_date..@end_date).where(@status).page(params[:page])
  end

  # GET /users/logs
  def logs
    drop_breadcrumb(t('breadcrumbs.user.logs'))
    @logs = if params[:query].present?
      users = current_agent.all_users.where("username LIKE ? OR nickname LIKE ?", "%#{params[:query]}%", "%#{params[:query]}%")
      PublicActivity::Activity.where(trackable: users).page(params[:page])
    else
      PublicActivity::Activity.where(trackable: current_agent.all_users).page(params[:page])
    end
  end

  private

  def user_params
    # params[:user][:user_game_platformship]['game_limit'] = params[:user][:user_game_platformship]['game_limit'].map(&:last).join(',')
    params.require(:user).permit(:username, :email, :nickname, :current_password, :password, :password_confirmation, :name, :mobile, :qq, user_game_platformship: [:video_limit, :roulette_limit, :game_limit])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
    @form = UserForm::Update.new(@user)
  end

  def valid_permission!
    authorize :user
  end

  def set_transfer_credit_type
    @status = "(`key` = 'user.transfer_credit') OR (`key` = 'user.deposit' AND `status` = 'transferred') OR (`key` = 'user.cashout' AND `status` IN ('transferred', 'removed')) OR (`key` = 'user.apply_cashout' AND `status` IN ('transferred', 'cancelled'))"
  end

end