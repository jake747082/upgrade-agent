class Schedules::BaseController < ApplicationController
  before_action :valid_permission!, :valid_not_api_site!

  protected

  def set_agent_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.schedules.racing'), schedules_racings_path)
  end

  def valid_permission!
    authorize :schedule, :all?
  end

  def valid_not_api_site!
    render_404! if Setting.api_site?
  end
end