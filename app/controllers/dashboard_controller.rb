require 'rqrcode'

class DashboardController < ApplicationController
  include LinkHelper

  # GET /
  def index
    @commission_reports = Finance::AgentCommissionView.call(current_agent, {})

    respond_to do |format|
      if current_agent.have_shop?
        format.html { render 'shop' }
        format.js {
          # index.js update machines credit used
          @boot_machines = Machine.list_boot.list_active
          @shutdown_machines = Machine.list_shutdown.list_active
        }
      else
        qrcode = RQRCode::QRCode.new(agent_promo_url(current_agent))
        @svg = qrcode.as_svg(offset: 0, color: '000', 
                    shape_rendering: 'crispEdges', 
                    module_size: 4)
        format.html { render 'agent' }
      end
    end
  end
end
