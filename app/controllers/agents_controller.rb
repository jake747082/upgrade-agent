class AgentsController < Agent::BaseController
  before_action :authorize_unlocked_agent!, only: [:edit, :update, :get_children]
  before_action :set_agent, except: :index

  # GET /agents
  def index
    @agents = Agent.except_me.search(params[:query]).page(params[:page])
  end

  # GET /agents/1
  def show
    respond_to do |format|
      # date
      from_date = Date.today - 1.month
      end_date = Date.today
      @logs = @agent.all_change_logs.where(key: "agent.cashout").date_range(from_date, end_date).page(1)
      @cashout_stats = Finance::DailyCashout.call(agent: @agent, begin_date: from_date, end_date: Date.today)
      format.html
      format.json {
        render "agents/show.json"
      }
    end
  end

  # GET /agents/1/edit
  def edit
    authorize :agent, :edit?
    @form = AgentForm::Update.new(@agent)
  end

  # PATCH/PUT /agents/1
  def update
    authorize :agent, :update?
    @form = AgentForm::Update.new(@agent)
    if @form.update(params[:agent])
      @agent.create_log(:update, current_account, remote_ip)
      redirect_to @agent, notice: t('flash.shared.update.finish', name: Agent.model_name.human)
    else
      render :edit
    end
  end

  def get_children
    respond_to do |format|
      format.json { 
        agent = @agent || current_account
        render json: agent.children.select(:id, :nickname).to_json
      }
    end
  end

  private

  def set_agent
    @agent = Agent.except_me.find(params[:id] || params[:agent_id])
    drop_breadcrumb(@agent.title) unless current_account.in_final_level?
  end
end