class Devise::BaseController < ApplicationController
  before_action :authorize_unlocked_agent!, only: [:edit, :update]
  before_action :setup_devise_strong_parameters, :set_breadcrumbs_path, :require_not_login!
  layout :set_layout_by_resource

  protected

  def require_not_login!
    redirect_to root_url if controller_name == 'sessions' && action_name == 'new' && agent_account_signed_in?
  end

  def setup_devise_strong_parameters
    devise_parameter_sanitizer.for(:account_update) { |u|
      u.permit(:password, :password_confirmation, :current_password)
    }
  end

  def set_layout_by_resource
    if controller_name == 'sessions' then false else 'application' end
  end

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.account'), edit_agent_registration_path) if controller_name == 'registrations'
  end
end