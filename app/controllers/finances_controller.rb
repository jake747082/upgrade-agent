class FinancesController < Agent::BaseController
  include DateRangeFilter
  before_action :valid_permission!, :set_default_date_range

  # GET /finances
  def index
    @lobby_id = params[:lobby_id]
    @agent = params[:agent].nil? || params[:agent].empty? ? current_agent : Agent.find(params[:agent])
    drop_breadcrumb(t('breadcrumbs.finances'))
    render 'agent/finances/index'
  end

  protected

  def valid_permission!
    authorize :finances, :all?
  end
end