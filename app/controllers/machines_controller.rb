class MachinesController < Machine::BaseController
  before_action :valid_permission!
  before_action :authorize_unlocked_agent!, only: [:destroy]
  before_action :set_machine, only: [:show, :destroy]

  # GET /machines
  def index
    @machines = Machine.list_active.includes(:agent).search(params[:query]).page(params[:page])
  end

  # GET /machines/1.json
  def show
    respond_to do |format|
      format.js {
        @machine_logs = @machine.cash_trade_logs.limit(10).includes(:trackable)
      }
    end
  end

  # DELETE /machines/1.json
  def destroy
    respond_to do |format|
      format.js {
        @form = MachineForm::Delete.new(@machine)
        @machine.create_log(:delete, current_account, remote_ip) if @status = @form.delete
      }
    end
  end

  # GET /machines/logs
  def logs
    drop_breadcrumb(t('breadcrumbs.user.logs'))
    @logs = if params[:query].present?
      machines = current_agent.all_machines.where("nickname LIKE ? OR mac_address = ?", "%#{params[:query]}%", "%#{params[:query]}%")
      PublicActivity::Activity.where(trackable: machines).page(params[:page])
    else
      PublicActivity::Activity.where(trackable: current_agent.all_machines).page(params[:page])
    end
  end

  protected

  def valid_permission!
    authorize :machine
  end

  def set_machine
    @machine = Machine.find(params[:id])
  end
end