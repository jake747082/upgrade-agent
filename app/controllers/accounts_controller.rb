class AccountsController < Account::BaseController
  before_action :authorize_unlocked_agent!, except: [:index]
  before_action :set_new_form, only: [:new, :create]
  before_action :set_account, only: [:edit, :update]

  # GET /accounts
  def index
    @accounts = current_agent.accounts
  end

  # GET /accounts/new
  def new
    drop_breadcrumb(t('breadcrumbs.sub_accounts.new'), accounts_path)
  end

  # POST /accounts
  def create
    if @form.create(params[:agent_sub_account])
      @form.model.create_log(:update, current_account, remote_ip)
      redirect_to accounts_path, notice: 'Success'
    else
      render :new
    end
  end

  # GET /accounts/1
  def edit
    @form = AgentForm::SubAccount::Update.new(@account)
    drop_breadcrumb(t('breadcrumbs.sub_accounts.edit'), accounts_path)
  end

  # PUT/PATCH /accounts/1
  def update
    @form = AgentForm::SubAccount::Update.new(@account)
    if @form.update(params[:agent_sub_account])
      @form.model.create_log(:update, current_account, remote_ip)
      redirect_to accounts_path, notice: 'Update successful'
    else
      render :edit
    end
  end

  protected

  def set_new_form
    @account = current_agent.accounts.new
    @form = AgentForm::SubAccount::Create.new(@account)
  end
end