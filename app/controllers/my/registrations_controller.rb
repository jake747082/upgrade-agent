class My::RegistrationsController < Devise::RegistrationsController

  protected

  def after_update_path_for(resource)
    edit_agent_registration_path
  end

  # 編輯個人資料
  def account_update_params
    params.require(:agent).permit(:current_password, :password, :password_confirmation, bank_account_attributes: [:security_code, :security_code_confirmation, :id])
  end

end
