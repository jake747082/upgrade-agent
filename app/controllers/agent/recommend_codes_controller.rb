class Agent::RecommendCodesController < Agent::BaseController
  before_action :valid_permission!, :authorize_unlocked_agent!, :set_agent, :set_service

  # POST /agents/:agent_id/recommend_codes
  def create
    if @service.reset!
      redirect_to agent_path(@agent), notice: t('flash.agent.recommend_code.finish')
    else
      redirect_to agent_path(@agent), alert: t('falsh.agent.error')
    end
  end

  protected

  def set_service
    @service = Agent::ControlRecommendCode.new(@agent)
  end
end