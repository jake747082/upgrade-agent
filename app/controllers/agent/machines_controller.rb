class Agent::MachinesController < Agent::BaseController
  before_action :valid_permission!, :set_parent_agent, :set_breadcrumbs_path

  # GET /agents/1/machines
  def index
    @machines = @parent_agent.all_machines.includes(:agent).page(params[:page])
    render 'machines/index'
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(t('breadcrumbs.machine.root'), machines_path)
    drop_breadcrumb(@parent_agent.title, agent_machines_path(@parent_agent))
  end

  def valid_permission!
    authorize :machine, :index?
  end
end
