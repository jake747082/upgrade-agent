class Agent::CashoutsController < Agent::BaseController
  before_action :authorize_unlocked_agent!, :set_agent

  # POST /agent/:agent_id/cashouts
  def create
    status = Agent::Cashout.call(@agent) do |agent, cashout_credit|
      if cashout_credit != 0
        agent.create_log(:cashout, current_account, request.ip,
                                 cashout_credit: cashout_credit)
      end
    end

    if status
      redirect_to @agent , notice: t('flash.machine.cashout.finish')
    else
      redirect_to @agent
    end
  end

  private

  def valid_permission!
    authorize :'agent/cashout', :create?
  end
end