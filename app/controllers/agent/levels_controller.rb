class Agent::LevelsController < Agent::BaseController
  # GET /agent/level_agents/:agent_level
  def show
    agent_level = params[:agent_level]
    if Agent.agent_levels[agent_level].present? && Agent.agent_levels[agent_level] > current_agent.agent_level_cd
      @agents = Agent.except_me.send("list_#{agent_level}").page(params[:page])
      render 'agents/index'
    else
      render_404!
    end
  end
end
