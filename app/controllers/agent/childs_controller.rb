class Agent::ChildsController < Agent::BaseController
  before_action :valid_permission!, :set_parent_agent, :valid_is_final_level_agent!, :set_breadcrumbs_path
  before_action :authorize_unlocked_agent!, only: [:new, :create]

  # GET /agent/1/childs
  def index
    @agents = @parent_agent.children.page(params[:page])
    render 'agents/index'
  end

  # GET /agent/1/childs/new
  def new
    @form = AgentForm::Create.new(@parent_agent.children.new)
    drop_breadcrumb(t('breadcrumbs.agent.new'))
  end

  # POST /agent/1/childs/new
  def create
    @form = AgentForm::Create.new(@parent_agent.children.new)
    if @form.create(params[:agent].merge(credit_max: 0))
      @form.model.create_log(:create, current_account, remote_ip)
      redirect_to @form.model, notice: t('flash.shared.create.finish')
    else
      render :new
    end
  end

  private

  def valid_permission!
    authorize :agent, :edit?
  end

  def set_breadcrumbs_path
    if @parent_agent.current_agent?
      drop_breadcrumb(@parent_agent.title)
    else
      drop_breadcrumb(@parent_agent.title, agent_path(@parent_agent))
    end
  end
end