class Agent::LogsController < Agent::BaseController
  before_action :set_agent

  # GET /agents/1/logs
  def index
    @logs = if params[:query].present?
      @agent.user_change_logs(params[:query]).page(params[:page])
    else
      @agent.action_logs.page(params[:page])
    end
  end

  # GET /agents/1/logs/change
  def change
    @logs = @agent.change_logs.where.not(owner_type: 'Admin').page(params[:page])
    render :index
  end

  private

  def valid_permission!
    authorize :'agent/log'
  end
end