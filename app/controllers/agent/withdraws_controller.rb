class Agent::WithdrawsController < Agent::BaseController
  include DateRangeFilter
  before_action :set_agent
  before_action :valid_permission!, :set_default_date_range

  # GET /agent/withdraws
  def index
    drop_breadcrumb(t('breadcrumbs.reports.withdraw.agent', agent: @agent.title), agent_withdraws_path(@agent))

    @withdraw_stats, @withdraw_total = Withdraw::ChildrenReport.call(begin_date: @begin_date, end_date: @end_date, agent: @agent)

    render 'reports/withdraws/index'
  end

  protected

  def valid_permission!
    authorize :report, :all?
  end
end