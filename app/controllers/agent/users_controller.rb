class Agent::UsersController < Agent::BaseController
  before_action :valid_permission!, :authorize_unlocked_agent!, :set_agent, :set_breadcrumbs_path

  # GET /agents/1/users
  def index
    @users = @agent.all_users.includes(:agent).page(params[:page])
    render 'users/index'
  end

  # GET /agents/1/users/new
  def new
    @form = UserForm::Create.new(User.new(agent: current_agent))
    drop_breadcrumb("#{t('breadcrumbs.user.new')}")
  end
  
  # POST /agents/1/users/create
  def create
    # Get 預設代理線
    default_agent = current_agent
    if current_agent.in_top_level?
      default_line = Agent::CreateDefaultLine.new(current_agent)
      default_agent = default_line.create!
    end
    if default_agent
      @form = UserForm::Create.new(User.new(agent: default_agent))
      if @form.create(params[:user])
        @form.model.create_log(:create, current_agent, remote_ip)
        redirect_to agent_users_path(default_agent), notice: t('flash.shared.create.finish')
      else
        render :new
      end
    else
      render :new
    end
  end

  private

  def set_breadcrumbs_path
    drop_breadcrumb(@agent.title, agent_users_path(@agent)) unless current_agent.in_final_level?
  end

  def valid_permission!
    authorize :user
  end
end