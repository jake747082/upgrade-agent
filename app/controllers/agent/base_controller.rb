class Agent::BaseController < ApplicationController
  before_action :valid_permission!, :set_agent_breadcrumbs_path

  private

  def set_agent_breadcrumbs_path
    return if @agent && @agent.current_agent?
    drop_breadcrumb(t('breadcrumbs.agent.root'), agents_path) unless current_account.in_final_level?
  end

  def set_parent_agent
    @parent_agent = Agent.find(params[:agent_id])
  end

  def set_agent
    @agent = Agent.find(params[:agent_id] || parmas[:id])
  end

  def valid_permission!
    authorize :agent, :all?
  end

  def valid_is_final_level_agent!
    render_404! if (@parent_agent || @agent).in_final_level?
  end
end