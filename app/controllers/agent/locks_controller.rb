class Agent::LocksController < Agent::BaseController
  before_action :valid_permission!, :authorize_unlocked_agent!, :set_agent
  before_action :set_agent_locking_service, only: [:create, :destroy]

  # POST /agents/:agent_id/locks
  def create
    status = @agent_lock_service.lock do |lock_target|
      lock_target.create_log(:lock, current_account, remote_ip)
    end
    if status
      redirect_to edit_agent_path(@agent), notice: t('flash.agent.locks.finish', total_agents_count: @agent.total_agents_count, total_machines_count: @agent.total_machines_count)
    else
      redirect_to edit_agent_path(@agent)
    end
  end

  # GET /agents/:agent_id/locks/destroy_confirm
  def destroy_confirm; end

  def parent_lock_notice; end

  # DELETE /agents/:agent_id/locks
  def destroy
    callback_proc = Proc.new { |unlock_target| unlock_target.create_log(:unlock, current_account, remote_ip) }
    operation_method = if params[:tree] then :unlock_all else :unlock end

    if @agent_lock_service.send(operation_method, &callback_proc)
      redirect_to edit_agent_path(@agent), notice: success_notice_message
    else
      redirect_to edit_agent_path(@agent)
    end
  end

  private

  def valid_permission!
    authorize :agent, :edit?
  end

  def set_agent_locking_service
    @agent_lock_service = Agent::TreeLock.new(@agent)
  end

  def success_notice_message
    if params[:tree] || @agent.in_final_level?
      t('flash.agent.unlock_tree.finish', total_agents_count: @agent.total_agents_count, total_machines_count: @agent.total_machines_count)
    else
      t('flash.agent.unlock.finish')
    end
  end
end
