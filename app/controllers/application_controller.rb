class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception unless Rails.env.test?
  [ Pundit, ExceptionRenderer, AgentAccounts, SiteHelper, LangSwitcher ].each { |m| include m }

  before_action :set_base_breadcrumbs
  layout 'application'
  define_site :agent

  protected

  # 重新設定 breadcrumbs path
  def set_base_breadcrumbs
    no_breadcrumbs
    drop_breadcrumb(t('breadcrumbs.root'), '/')
  end

  def valid_date!
    redirect_to :back , alert: t('flash.logs.date_diff') if diff_days(@begin_date, @end_date) >= 31
  end

  def diff_days(begin_date, end_date)
    (end_date.to_datetime - begin_date.to_datetime).to_f
  end

  # messages_params: {
  #  title: '',
  #  content: '',
  #  username: '',
  #  groups: []
  # }
  def send_message(messages_params)
    message_form = MessageForm::Create.new(Message.new)
    if message_form.create(messages_params)
      true
    else
      false
    end
  end
end
