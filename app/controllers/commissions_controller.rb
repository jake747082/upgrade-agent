class CommissionsController < Agent::BaseController
  before_action :valid_permission!
  before_action :setup_form_data, only: [:users, :details]
  before_action :set_drop_breadcrumb, except: :show # 要放setup_form_data後面

  def index
    @logs = current_agent.agent_commission_logs.page(params[:page])
  end

  def show
    @log = current_agent.agent_commission_logs.find(params[:id])
    @game_rate = Reports::AgentCommission.game_rate
    # 起始日期
    @begin_date = @log.begin_date.at_beginning_of_month
    @end_date = @log.end_date.at_end_of_month
    @commission_reports = Finance::AgentCommissionView.call(current_agent, {begin_date: @begin_date, end_date: @end_date})
    set_drop_breadcrumb
  end

  def users
    effective_bet_users = current_agent.third_party_bet_forms.effective_bet_users(current_agent.id, "#{@begin_date} 00:00:00", "#{@end_date} 23:59:59").pluck(:user_id)
    @users = current_agent.users.search_users(effective_bet_users).page(params[:page])
  end

  def details
    @sum = {deposit: {credit_actual: 0, credit: 0}, cashout: {credit_actual: 0, credit: 0}}
    @details = current_agent.withdraws.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).page(params[:page])
    @sum[:deposit][:credit_actual] = current_agent.withdraws.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(cash_type_cd: 0).sum(:credit_actual)
    @sum[:deposit][:credit] = current_agent.withdraws.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(cash_type_cd: 0).sum(:credit)
    @sum[:cashout][:credit_actual] = current_agent.withdraws.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(cash_type_cd: 1).sum(:credit_actual)
    @sum[:cashout][:credit] = current_agent.withdraws.list_by_type(params[:type]).where(updated_at: @begin_date..@end_date).where(cash_type_cd: 1).sum(:credit)
  end

  private

  def set_drop_breadcrumb
    drop_breadcrumb(t('common.navigation_bar.my.commissions'), commissions_path)
    drop_breadcrumb("#{@begin_date} - #{@end_date}") if !@begin_date.nil? && !@end_date.nil?
  end

  protected

  def valid_permission!
    authorize :commissions, :all?
  end

  def setup_form_data
    @begin_date = params[:begin_date]
    @end_date = params[:end_date]
  end
end