module AgentAccounts
  extend ActiveSupport::Concern

  included do
    if self == ApplicationController
      around_action :scope_current_agent
      before_action :authenticate_agent_and_account!
    end

    helper_method :current_account, :current_agent, :agent_account_signed_in?
    alias_method :pundit_user, :current_account
  end

  protected

  def agent_account_signed_in?
    @agent_account_signed_in ||= (agent_sub_account_signed_in? || agent_signed_in?)
  end

  def authenticate_agent_and_account!
    authenticate_agent! unless agent_account_signed_in?
  end

  def current_account
    @current_account ||= (current_agent_sub_account || current_agent)
  end

  def current_agent
    @current_agent ||= (if agent_sub_account_signed_in? then current_agent_sub_account.agent else super end)
  end

  def scope_current_agent
    Registry.current_agent = current_agent
    yield
  ensure
    Registry.current_agent = nil
  end

  def authorize_unlocked_agent!
    authorize :common, :agent_unlocked?
  end
end