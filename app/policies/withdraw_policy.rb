class WithdrawPolicy < CommonPolicy
  def all?
    # Setting.cash_site? && account.is_points_type? && account.in_final_level?
    false
  end
  def agent?
    Setting.cash_site? && account.is_points_type? && !account.in_final_level? && account.agent_write_permission?
  end
end