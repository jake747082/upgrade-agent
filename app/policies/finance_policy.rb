class FinancePolicy < CommonPolicy
  def all?
    finances?
  end
end