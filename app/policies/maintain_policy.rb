class MaintainPolicy < CommonPolicy
  def all?
    account.in_final_level?
  end
end