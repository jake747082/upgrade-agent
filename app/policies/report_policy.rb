class ReportPolicy < CommonPolicy
  def all?
    finances?
  end
end