class MachinePolicy < CommonPolicy

  def index?
    machines_list?
  end

  def show?
    true
  end

  def logs?
    machines_list?
  end

  def destroy?
    machines_control? && account.in_final_level?
  end
end