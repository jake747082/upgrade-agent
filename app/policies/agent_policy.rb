class AgentPolicy < Agent::BasePolicy
  alias_method :show?, :all?
  alias_method :index?, :all?

  def edit?
    @edit ||= all? && account.agent_write_permission?
  end
  alias_method :update?, :edit?
end