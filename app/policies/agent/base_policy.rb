class Agent::BasePolicy < CommonPolicy
  def all?
    @all ||= agents_list? && account.in_final_level? == false
  end
end