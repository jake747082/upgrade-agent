class Agent::UserPolicy < Agent::BasePolicy
  def index?
    users_read?
  end

  def new?
    account.in_final_level?
    # users_write? && account.is_points_type? # && Setting.shop_site?
  end

  def create?
    account.in_final_level?
    # users_write? && account.is_points_type?# && Setting.shop_site?
  end
end