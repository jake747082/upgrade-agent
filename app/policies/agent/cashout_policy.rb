class Agent::CashoutPolicy < Agent::BasePolicy
  def create?
    @create ||= account.agent.director? && account.agent_write_permission?
  end
end