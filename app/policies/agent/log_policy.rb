class Agent::LogPolicy < Agent::BasePolicy
  def index?
    @index ||= (agents_list? || account.in_final_level?)
  end

  alias_method :change?, :index?
end