class CommissionPolicy < CommonPolicy
  def all?
    Setting.cash_site? && account.agent.is_cash_type? && account.agent.in_final_level?
  end
end