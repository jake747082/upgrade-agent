class CommonPolicy < ApplicationPolicy

  def machines_list?
    @machines_list ||= account.machine_read_permission? && Setting.shop_site?
  end

  def machines_control?
    @machines_control ||= account.machine_write_permission? && account.in_final_level? && Setting.shop_site?
  end

  def users_read?
    @users_read ||= account.machine_read_permission? && !Setting.shop_site?
  end

  def users_write?
    @users_write ||= account.machine_write_permission? && !Setting.shop_site?
  end

  def agents_list?
    account.agent_read_permission?
  end

  def finances?
    account.finances_permission?
  end

  def agent_unlocked?
    !account.agent.lock? && !account.lock?
  end

  def sub_accounts?
    account.sub_account_permission?
  end

  alias_method :game_history?, :finances?
end