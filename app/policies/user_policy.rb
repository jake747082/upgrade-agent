class UserPolicy < CommonPolicy
  def index?
    users_read?
  end

  def show?
    users_write? && account.user_permission?
  end

  def edit?
    users_write? && account.user_permission?
  end

  def new?
    (account.in_top_level? || account.in_final_level?) && account.user_permission?
  end

  # 上分 / 下分
  def edit?
    account.user_permission? && account.is_points_type?
  end

  def logs?
    users_read?
  end

  def limit_table?
    true
  end

  def transfer_credit?
    users_read?
  end

  def points?
    users_write? && account.user_permission? && account.is_points_type? && account.in_final_level?
  end
  alias_method :create?, :new?
  alias_method :update?, :edit?
end