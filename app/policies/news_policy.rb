class NewsPolicy < CommonPolicy
  def all?
    Setting.cash_site?
  end
end