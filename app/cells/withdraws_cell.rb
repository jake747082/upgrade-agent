class WithdrawsCell < BaseCell
  helper ReportHelper

  # 日期區間選擇
  def filter_form(args)
    setup_form_data(args)
    render
  end

  # 報表顯示
  def withdraw_list(args)
    setup_form_data(args)
    @withdraw_stats = Withdraw::ChildrenReport.call(begin_date: @begin_date, end_date: @end_date, agent: @agent)
    render
  end

  # 報表顯示
  def user_withdraw_list(args)
    setup_form_data(args)
    @withdraw_stats = Withdraw::UserReport.call(begin_date: @begin_date, end_date: @end_date, user: @user)
    render
  end

  private

  def setup_form_data(args)
    @begin_date = args[:begin_date]
    @end_date = args[:end_date]
    @agent = args[:agent] || nil
    @user = args[:user] || nil
  end
end