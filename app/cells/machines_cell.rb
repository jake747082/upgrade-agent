class MachinesCell < BaseCell
  append_view_path "app/views"

  def log(args)
    @log = args[:log]
    render
  end
end