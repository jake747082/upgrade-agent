class CommonCell < BaseCell
  helper ApplicationHelper

  def navigation_bar
    render
  end

  def menu
    render
  end
end