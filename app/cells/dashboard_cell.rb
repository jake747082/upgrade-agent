class DashboardCell < BaseCell
  helper DashboardHelper

  # machine deposit/cashout logs panel
  def logs_panel
    @machine_logs = current_agent.all_action_logs.where(trackable_type: 'Machine', key: ['machine.cashout', 'machine.deposit']).limit(10)
    render
  end

  # boot/shutdown machines panel
  def machines_panel(args)
    @machines = if args[:action] == 'boot'
      Machine.list_boot.list_active
    else
      Machine.list_shutdown.list_active
    end
    @title = args[:title]
    @style = args[:style]
    @action = args[:action]
    @panel_id = args[:panel_id]
    @machine_counter_id = args[:machine_counter_id]
    render
  end

  # machine row
  def machine(args)
    @machine = args[:machine]
    render
  end

  # agent (not shop) dashboard panel
  def summary
    @total_machines_count = current_agent.total_machines_count if Setting.shop_site?
    @total_agents_count = current_agent.total_agents_count unless current_agent.in_final_level?
    @total_users_count = current_agent.total_users_count if Setting.cash_site?
    @remote_ip = controller.request.ip
    render
  end

  # current agent profit panel
  def profit_overview
    # @today_win_amount  = SlotBetForm.today.total_agents_win_amount + RacingBetForm.today.total_agents_win_amount
    # @today_self_profit = SlotBetForm.today.total_self_profit(current_agent) + RacingBetForm.today.total_self_profit(current_agent)
    # @today_total_cash  = SlotBetForm.today.total_cash + RacingBetForm.today.total_cash
    # @today_owe_parent  = @today_win_amount - @today_self_profit
    # @total_jackpot     = SlotMachine.total_jackpot
    render
  end

  def report_overview
    if params[:type].nil? && params[:begin_date].nil? && params[:end_date].nil?
      @today_users_count = User.today.count
      @week_users_count = User.week.count
      @month_users_count = User.month.count
  
      @today_total_betform_sum = ThirdPartyBetForm.today.total_betform_sum
      @week_total_betform_sum = ThirdPartyBetForm.week.total_betform_sum
      @month_total_betform_sum = ThirdPartyBetForm.month.total_betform_sum

      @today_total_withdraw_sum = {}
      @week_total_withdraw_sum = {}
      @month_total_withdraw_sum = {}
      Withdraw.list_transferred.today.total_withdraw_sum.each { |sum| @today_total_withdraw_sum[sum.cash_type] = sum.total_credit }
      Withdraw.list_transferred.week.total_withdraw_sum.each { |sum| @week_total_withdraw_sum[sum.cash_type] = sum.total_credit }
      Withdraw.list_transferred.month.total_withdraw_sum.each { |sum| @month_total_withdraw_sum[sum.cash_type] = sum.total_credit }
    else
      @begin_date = params[:begin_date]
      @end_date = params[:end_date]
      @users_count = User.datetime_range(@begin_date, @end_date).count
      @total_betform_sum = ThirdPartyBetForm.datetime_range(@begin_date, @end_date).total_betform_sum
      @total_withdraw_sum = {}
      Withdraw.list_transferred.datetime_range(@begin_date, @end_date).total_withdraw_sum.each { |sum| @total_withdraw_sum[sum.cash_type] = sum.total_credit }
    end
    render
  end

  def commission(args)
    @agent = current_agent
    @game_rate = Reports::AgentCommission.game_rate
    @commission_reports = args[:commission_reports]
    render
  end
end