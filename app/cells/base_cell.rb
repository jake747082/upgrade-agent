class BaseCell < Cell::Rails
  include Pundit
  include Devise::Controllers::Helpers
  include AgentAccounts
end