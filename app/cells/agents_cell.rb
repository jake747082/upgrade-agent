class AgentsCell < BaseCell

  def filter_bar(args)
    @value = args[:value]
    render
  end

  def information(args)
    @agent = args[:agent]
    render
  end
end
