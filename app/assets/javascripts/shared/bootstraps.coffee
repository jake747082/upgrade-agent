# @NOTICE 不要一次載入整包 assets pipeline compile 會很慢
#= require twitter/bootstrap/alert
#= require twitter/bootstrap/button
#= require twitter/bootstrap/modal
#= require twitter/bootstrap/tooltip
#= require twitter/bootstrap/dropdown
#= require twitter/bootstrap/collapse

#= require bootstrap-datepicker/core
#= require bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW

#= require bootstrap-datetimepicker.min
