# jQuery & Plugins
#= require jquery
#= require jquery_ujs
#= require jquery.ui.all
#= require shared/bootstraps

# Plugins
#= require pusher
#= require turbolinks
#= require nprogress
#= require nprogress-turbolinks
#= require zeroclipboard

# Others
#= require shared/pusher
#= require autocomplete-rails