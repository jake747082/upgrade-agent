$(function() {
  // 網咖dahboard固定時間更新顯示資料
  $(function() {
    if ($('#shop-dashboard').size() > 0) {
      $.dashboardPolling = setInterval(function() {
        if ($('#shop-dashboard').size() > 0) {
          $.getScript($(this).data('url'));
        } else {
          clearInterval($.dashboardPolling);
        }
      }, 2500);
    }
  });
  // 設定dashboard每個功能區塊加上捲軸
  var setScollbar = function($panel, height) {
    if ($panel.height() > height) {
      $panel.height(height).css({
        overflow: "auto"
      });
    }
  };
  // enter之後close modal
  var enterCloseModal = function() {
    $('body').on('keypress', '.modal-enter-close', function(e) {
      if (e.which == 13) {
        e.preventDefault();
        $('.run-and-close-modal').click();
      }
    });
  };
  // modal auto focus input
  var autoFocusModal = function() {
    $('body').on('shown.bs.modal', '.modal', function() {
      $(this).find("[autofocus]:first").focus();
    });
  };
  enterCloseModal();
  autoFocusModal();
  logsPanelHeight = $(window).height() - $('.navbar').height() - $('#marquee').height() - $('#profit').height() - 105;
  machinePanelHeight = (logsPanelHeight - 60) / 2;

  setScollbar($('#machine-logs-panel'), logsPanelHeight);
  setScollbar($('#boot-machines-panel'), machinePanelHeight);
  setScollbar($('#shutdown-machines-panel'), machinePanelHeight);

  if (typeof($.dashboard) == 'undefined') {
    var DashBoardHelper = function() {
      var self = this;
      this.elems = {};
      this.cacheElements = function() {
        self.elems = {
          bootMachinesCounter: $('#boot-machine-count'),
          shutdownMachinesCounter: $('#shutdown-machine-count'),
          machineLogsTbody: $('#machine-logs-panel tbody'),
          bootMachinesPanel: $('#boot-machines-panel'),
          shutdownMachinesPanel: $('#shutdown-machines-panel')
        };
      };
      this.cacheElements();
      // 遞增 info counter
      this.incrumentMachineCounter = function(status) {
        if (status == 'boot') {
          self.elems.bootMachinesCounter.text(parseInt(self.elems.bootMachinesCounter.text(), 10) + 1);
          self.elems.shutdownMachinesCounter.text(parseInt(self.elems.shutdownMachinesCounter.text(), 10) - 1);
        } else {
          self.elems.bootMachinesCounter.text(parseInt(self.elems.bootMachinesCounter.text(), 10) - 1);
          self.elems.shutdownMachinesCounter.text(parseInt(self.elems.shutdownMachinesCounter.text(), 10) + 1);
        }
      };

      // 寫入 log
      this.addMachineLog = function(partialHTML) {
        $logRow = $(partialHTML).addClass('danger');
        self.elems.machineLogsTbody.prepend($logRow);
        setTimeout(function() {
          $logRow.removeClass('danger');
        }, 3000);
        if ($('#machine-logs-panel .public_activity_activity').size() > 0) {
          $('#machine-logs-panel .no-data').addClass('hidden');
        }
      };

      // 當變更machine開機/關機時, 需變換至開機/關機位置
      this.controlMachine = function(action, id, partialHTML) {
        $('#machine_' + id).remove();
        $machineRow = $(partialHTML);
        if (action == 'boot') {
          self.elems.bootMachinesPanel.find('tbody').prepend($machineRow);
        } else {
          self.elems.shutdownMachinesPanel.find('tbody').prepend($machineRow);
        }
        reloadEmptyMachineTable();
      };

      // 更新該機器資料
      this.updateMachine = function(id, partialHTML) {
        $machineRow = $(partialHTML);
        $('#machine_' + id).html($machineRow.html());
      };

      // 判斷是否區塊無資料, 控制無資料訊息隱藏/顯示
      var reloadEmptyMachineTable = function() {
        if (self.elems.bootMachinesPanel.find('.machine').size() === 0) {
          self.elems.bootMachinesPanel.find('.no-data').removeClass('hidden');
        } else {
          self.elems.bootMachinesPanel.find('.no-data').addClass('hidden');
        }
        if (self.elems.shutdownMachinesPanel.find('.machine').size() === 0) {
          self.elems.shutdownMachinesPanel.find('.no-data').removeClass('hidden');
        } else {
          self.elems.shutdownMachinesPanel.find('.no-data').addClass('hidden');
        }
      };

      // 比較值是否已改變並更新
      var updateValue = function(id, value) {
        if ($('#' + id).html() != value) {
          $('#' + id).html(value).addClass('font-danger');
          setTimeout(function() {
            $('#' + id).removeClass('font-danger');
          }, 3000);
        }
      };
      // 更新information
      this.updateInformation = function(values) {
        $.each(values, function(id, value) {
          updateValue(id, value);
        });
      };
      // 更新boot/shutdown 機器creadit
      this.updateMachineCredit = function(id, value) {
        $machineCredit = $('#machine_' + id + ' .credit-left');
        if ($machineCredit.text() != value) {
          $machineCredit.text(value).addClass('font-bold');
          setTimeout(function() {
            $machineCredit.removeClass('font-bold');
          }, 3000);
        }
      };
    };
    $.dashboard = new DashBoardHelper();
  } else {
    $.dashboard.cacheElements();
  }
});