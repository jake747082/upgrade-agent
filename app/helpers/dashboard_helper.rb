module DashboardHelper
  def render_maintain_button(current_agent)
    if current_agent.maintain_open?
      maintain_on_button
    else
      maintain_off_button
    end
  end

  private

  def maintain_on_button
    link_to(%(<span class="fa fa-lock font-on"></span> #{t(".maintain_open")}).html_safe, new_maintains_path, id: 'maintain', class: 'btn btn-default btn-block')
  end

  def maintain_off_button
    link_to(%(<span class="fa fa-lock font-off"></span> #{t('.maintain_close')}).html_safe, new_maintains_path, id: 'maintain', class: 'btn btn-default btn-block')
  end
end