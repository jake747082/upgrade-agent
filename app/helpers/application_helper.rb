module ApplicationHelper
  # 在當頁nav link加上active class
  def nav_link(link_text, link_path, opt = {})
    class_name = current_page?(link_path) ? ' active' : ''
    if opt[:class].nil?
      opt[:class] ||= class_name
    else
      opt[:class] += class_name
    end
    unless opt[:count].nil?
      link_text = "#{link_text} <span class='badge badge-warning'>#{opt[:count]}<span>"
    end
    content_tag(:li, opt) do
      link_to link_text.html_safe, link_path
    end
  end

  def current_group?(controller_group)
    controller = params[:controller].split('/')
    controller[0] == controller_group
  end

  def update_platform_time(type)
    return '' if type == 'all'
    type = type == 'mg' || type == 'bbin' || type.nil? ? 'ag' : type
    default_log_time(GamePlatform.find_by_name(type).updated_at)
  end
  
  def default_zero(value, key)
    value.nil? ? 0 : value[key]
  end
end