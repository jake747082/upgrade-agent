Gambling::Application.routes.draw do
  # Concerns
  concern :loggable do |options|
    resources :logs, only: [:index], module: options[:module] do
      collection do
        get :change
      end
    end
  end

  devise_for :agents, path: :my, skip: [:registrations, :passwords, :confirmations]
  devise_scope :agent do
    resource :registration, only: [:edit, :update],
      path: :my,
      path_names: { edit: 'edit' },
      controller: 'my/registrations',
      as: :agent_registration
  end

  devise_for :agent_sub_accounts, path: :sub_account, skip: [:registrations, :passwords, :confirmations]
  devise_scope :agent_sub_account do
    resource :registration, only: [:edit, :update],
      path: :sub_account,
      path_names: { edit: 'edit' },
      controller: 'devise/registrations',
      as: :agent_sub_account_registration
  end

  # 1. 主畫面
  root 'dashboard#index'

  namespace :dashboard do
    get :site_map
  end

  # 2. 代理階層
  namespace :agent do
    resource :levels, only: :show
  end

  resources :finances, only: :index
  resources :commissions, only: [:index, :show] do 
    get :users, on: :collection
    get :details, on: :collection
  end

  resources :logs, only: :index

  namespace :log do
    resources :agent_withdraws, only: [:index, :show]
  end
  
  # 玩家提款處理
  resources :withdraws, only: [:index, :show, :update, :destroy]

  namespace :withdraw do
    # 代理買分
    resources :agent_deposits, only: [:new, :create] do
      get :autocomplete_agent_username, on: :collection
    end
    resources :agent_cashouts, only: [:new, :create] do
      get :autocomplete_agent_username, on: :collection
    end
    resources :agent_withdraws, only: [:index, :show]
  end

  # 3. 代理階層
  resources :agents, only: [:edit, :update, :index, :show] do
    # a. 玩家
    resources :users, only: [:index, :new, :create], module: :agent
    # b. 子經銷
    resources :childs, only: [:index, :new, :create], module: :agent
    # c. 機器
    resources :machines, only: :index, module: :agent
    # d. 財務
    resources :finances, only: :index, module: :agent do
      get :export, on: :collection
    end
    # e. logger
    concerns :loggable, module: :agent
    # f. 結帳
    resources :cashouts, only: [:create], module: :agent
    # g. 鎖定
    resource :locks, only: [:create, :destroy], module: :agent do
      get :destroy_confirm
      get :parent_lock_notice
    end
    # 充提報表
    resources :withdraws, only: :index, module: :agent
    # 產生推薦碼
    resource :recommend_codes, only: :create, module: :agent
    # finances select 取得下層
    get :get_children
  end

  # 4. 玩家管理
  resources :users, only: [:edit, :update, :index, :show, :new, :create] do
    get :limit_table, on: :collection
    get :logs, module: :user, on: :collection
    get "transfer_credit" => :transfer_credit
    resources :deposits, only: [:new, :create], module: :user
    resources :cashouts, only: [:new, :create], module: :user
    resources :details, only: :index, module: :user
  end

  # 5. 子帳號管理
  resources :accounts, except: [:show, :destory] do
    resource :locks, only: [:create, :destroy], module: :account
    concerns :loggable, module: :account
  end

  # 6. 機器管理
  resources :machines, only: [:index, :show, :destroy] do
    resources :cashouts, only: [:new, :create], module: :machine do
      get :withdraw, on: :collection
    end
    get :logs, module: :machine, on: :collection
    resources :deposits, only: [:new, :create], module: :machine
    resource :controls, only: [:create, :destroy], module: :machine
    concerns :loggable, module: :machine
  end

  # 7. 維護模式
  resource :maintains, only: [:new, :create, :destroy], module: :machine, on: :collection

  # 8. Pusher 認證
  resource :channels, only: :create

  # 9. 遊戲歷程
  namespace :histories do
    # resources :main, only: :index do
    #   get :export, on: :collection
    # end
    # resources :slot, only: :index do
    #   get :export, on: :collection
    # end
    # resources :little_mary, only: :index do
    #   get :export, on: :collection
    # end
    # resources :racing, only: [:index, :show] do
    #   get :export, on: :collection
    # end
    # resources :poker, only: [:index, :show] do
    #   get :export, on: :collection
    # end
    # resources :fishing_joy, only: [:index, :show] do
    #   get :export, on: :collection
    # end
    # resources :roulette, only: [:index, :show] do
    #   get :export, on: :collection
    # end
    resources :game, only: [:index, :show] do
      get :export, on: :collection
      get :game_result, on: :collection
    end
    # resources :electronic_game, only: [:index, :show] do
    #   get :export, on: :collection
    # end
  end

  # 10. 場次
  namespace :schedules do
    resources :racings, only: :index
  end

  # 最新公告
  resources :news, only: [:index, :show]
end
