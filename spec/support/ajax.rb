module AjaxHelper
  def wait_for_ajax_then
    sleep(0.1)
    yield if block_given?
  end
end