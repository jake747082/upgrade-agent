module LoginProcess
  def sign_in_as(agent)
    if agent.is_a?(Agent)
      visit '/my/sign_in'
    else
      visit '/sub_account/sign_in'
    end
    within("form#auth_form") do
      fill_in '帳號', with: agent.username
      fill_in '密碼', with: '888888'
    end
    click_button '登入'
  end
end