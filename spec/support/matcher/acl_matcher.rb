RSpec::Matchers.define :be_access_denied do |alert_message: '您沒有存取該資源的權限。'|
  match do |object|
    case object
    when ActionController::TestResponse
      case object.content_type
      when "text/javascript"
        @alert = object.body
        object.status == 403 && object.body.include?(alert_message)
      when "text/html"
        @alert = object.request.flash[:alert]
        object.status == 302 && object.request.flash[:alert] == alert_message
      else
        false
      end
    when Capybara::Session
      @alert = alert_message
      object.status_code == 200 && object.html.include?(alert_message)
    else
      false
    end
  end

  failure_message do |object|
    if object.is_a?(ActionController::TestResponse)
      "expected that 302 with alert: #{alert_message} but get #{object.status} with alert: #{@alert}"
    elsif object.is_a?(Capybara::Session)
      "expected that 302 with alert: #{alert_message} but get #{object.status_code} without alert: #{@alert}"
    else
      "Not an request object (Capybara::Session or ActionController::TestResponse)"
    end
  end
end

RSpec::Matchers.define :be_access_allow do |alert_message: '您沒有存取該資源的權限。'|
  match do |object|
    case object
    when ActionController::TestResponse
      case object.content_type
      when "text/javascript"
        @alert = object.body
        object.status == 200 && !object.body.include?(alert_message)
      when "text/html"
        @alert = object.request.flash[:alert]
        object.status == 200 && object.request.flash[:alert] != alert_message
      else
        false
      end
    when Capybara::Session
      @alert = alert_message
      object.status_code == 200 && !object.html.include?(alert_message)
    end
  end

  failure_message do |object|
    if object.is_a?(ActionController::TestResponse)
      "expected that 200 without alert: #{alert_message} but get #{object.status} with alert: #{@alert}"
    elsif object.is_a?(Capybara::Session)
      "expected that 200 without alert: #{alert_message} but get #{object.status_code} with alert: #{@alert}"
    else
      "Not an request object (Capybara::Session or ActionController::TestResponse)"
    end
  end
end