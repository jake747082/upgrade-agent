RSpec::Matchers.define :be_render_404 do |alert_message: '很抱歉, 該頁面不存在!'|
  match do |object|
    status_code, body = case object
    when ActionController::TestResponse
      [object.status, object.body]
    when Capybara::Session
      [object.status_code, object.html]
    else
      [nil, nil]
    end
    return status_code == 404 && body.include?(alert_message)
  end

  failure_message do |object|
    if object.is_a?(ActionController::TestResponse) || object.is_a?(Capybara::Session)
      "expected that 404 with alert: 很抱歉, 該頁面不存在!"
    else
      "Not an request object (Capybara::Session or ActionController::TestResponse)"
    end
  end
end