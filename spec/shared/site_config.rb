shared_context "site config" do
  before(:all) { @site_config = SiteConfig.create! }

  let(:site_config) { @site_config }
end

shared_context "cash site enabled" do
  before { allow(Setting).to receive(:cash_site?) { true } }
end

shared_context "cash site disabled" do
  before { allow(Setting).to receive(:cash_site?) { false } }
end

shared_context "shop site enabled" do
  before { allow(Setting).to receive(:shop_site?) { true } }
end

shared_context "shop site disabled" do
  before { allow(Setting).to receive(:shop_site?) { false } }
end
