RSpec.shared_context "slot game" do
  before(:all) {
    # Slot Game
    @slot_money_kindom = SlotMachine.create(game_model_name: 'Slot::MoneyKindom',
                                            lose_rate_xs: 0.65, lose_trigger_amount_xs: 20,
                                            lose_rate_sm: 0.17, lose_trigger_amount_sm: 80,
                                            lose_rate_md: 0.08, lose_trigger_amount_md: 200,
                                            lose_rate_lg: 0.04, lose_trigger_amount_lg: 500,
                                            lose_rate_xl: 0.01, lose_trigger_amount_xl: 1000,
                                            win_speed: 20)


    @slot_mayan = SlotMachine.create(game_model_name: 'Slot::MayanEclipse',
                                     lose_rate_xs: 0.65, lose_trigger_amount_xs: 20,
                                     lose_rate_sm: 0.17, lose_trigger_amount_sm: 80,
                                     lose_rate_md: 0.08, lose_trigger_amount_md: 200,
                                     lose_rate_lg: 0.04, lose_trigger_amount_lg: 500,
                                     lose_rate_xl: 0.01, lose_trigger_amount_xl: 1000,
                                     win_speed: 20)

    @slot_forbidden_city = SlotMachine.create(game_model_name: 'Slot::ForbiddenCity',
                                              lose_rate_xs: 0.65, lose_trigger_amount_xs: 20,
                                              lose_rate_sm: 0.17, lose_trigger_amount_sm: 80,
                                              lose_rate_md: 0.08, lose_trigger_amount_md: 200,
                                              lose_rate_lg: 0.04, lose_trigger_amount_lg: 500,
                                              lose_rate_xl: 0.01, lose_trigger_amount_xl: 1000,
                                              win_speed: 20)
    @slot_valley_kings = SlotMachine.create(game_model_name: 'Slot::ValleyKings',
                                            lose_rate_xs: 0.65, lose_trigger_amount_xs: 20,
                                            lose_rate_sm: 0.17, lose_trigger_amount_sm: 80,
                                            lose_rate_md: 0.08, lose_trigger_amount_md: 200,
                                            lose_rate_lg: 0.04, lose_trigger_amount_lg: 500,
                                            lose_rate_xl: 0.01, lose_trigger_amount_xl: 1000,
                                            win_speed: 20)

    @slot_ninja = SlotMachine.create(game_model_name: 'Slot::SamuraiVsNinja',
                                     lose_rate_xs: 0.65, lose_trigger_amount_xs: 20,
                                     lose_rate_sm: 0.17, lose_trigger_amount_sm: 80,
                                     lose_rate_md: 0.08, lose_trigger_amount_md: 200,
                                     lose_rate_lg: 0.04, lose_trigger_amount_lg: 500,
                                     lose_rate_xl: 0.01, lose_trigger_amount_xl: 1000,
                                     win_speed: 20)

    @slot_treasure = SlotMachine.create(game_model_name: 'Slot::TreasureOfTheTitans',
                                        lose_rate_xs: 0.65, lose_trigger_amount_xs: 20,
                                        lose_rate_sm: 0.17, lose_trigger_amount_sm: 80,
                                        lose_rate_md: 0.08, lose_trigger_amount_md: 200,
                                        lose_rate_lg: 0.04, lose_trigger_amount_lg: 500,
                                        lose_rate_xl: 0.01, lose_trigger_amount_xl: 1000,
                                        win_speed: 20)

    # Game Setting
    @game_setting = GameSetting.create(jackpot_primary_max: 2000000, jackpot_accumulate_rate: 0.02,
                                       jackpot_md_range_begin: 1000, jackpot_md_range_end: 3000, jackpot_md_interval: 200,
                                       jackpot_sm_range_begin: 200, jackpot_sm_range_end: 1000, jackpot_sm_interval: 50)

  }

  let(:slot_money_kindom) { @slot_money_kindom }
  let(:slot_mayan) { @slot_mayan }
  let(:slot_forbidden_city) { @slot_forbidden_city }
  let(:game_setting) { @game_setting }
end