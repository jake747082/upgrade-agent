RSpec.shared_context "slot spin" do
  before(:all) {
    @machine.update!(credit_max: 2000, credit_used: 0)
    @service = Slot::Spin.new(@slot_mayan, @machine)
    @status = @service.spin!(10, 25)
  }

  let(:service) { @service }
end

RSpec.shared_context "slot histroy calculator" do
  before(:all) {
    SlotMachineHistory.delete_all
    @histroy = @machine.slot_histories.find_or_create_by(slot_machine: @slot_mayan)
    @slot_histories_calculator = Slot::Spin::HistroyCalculator.new(@slot_mayan, @histroy)
  }
  let(:calculator) { @slot_histories_calculator }
  let(:histroy) { calculator.histroy }
end