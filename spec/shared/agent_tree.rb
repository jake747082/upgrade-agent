shared_context "agent tree and machine" do
  before { allow(Pusher).to receive(:trigger_async) { true } }

  before(:all) {
    Agent.delete_all
    Machine.delete_all

    # (股東)
    @shareholder = Agent.new(credit_max: 0)
    AgentForm::Create.new(@shareholder).create(username: 'agent1', nickname: '測試股東', password: '888888', password_confirmation: '888888', credit_max: 100000, casino_ratio: 95)

    # (總代)
    @director = Agent.new(credit_max: 0, parent: @shareholder)
    AgentForm::Create.new(@director).create(username: 'agent2', nickname: '測試總代', password: '888888', password_confirmation: '888888', credit_max: 80000, casino_ratio: 80)

    # (代理)
    @agent = Agent.new(credit_max: 0, parent: @director)
    AgentForm::Create.new(@agent).create(username: 'agent3', nickname: '測試代理', password: '888888', password_confirmation: '888888', credit_max: 20000, casino_ratio: 50)

    # 機器
    @machine = Machine.new(credit_max: 10000000, boot: false)
    @machine_form = MachineForm::Create.new(@machine).tap { |f| f.create(nickname: 'Machine1', mac_address: '3c-15-c2-d4-a6-b0', maintain_code: @agent.generate_maintain_code) }    
  }

  let(:shareholder) { @shareholder.reload }
  let(:director) { @director.reload }
  let(:agent) { @agent.reload }
  let(:machine) { @machine.reload }
  let(:machine_form) { @machine_form.reload }
end

shared_context "user accounts" do
  before(:all) {
    @user = UserForm::Create.new(User.new(agent: @agent)).tap { |f| f.create(username: 'user_1', nickname: 'user_1', password: 'password', password_confirmation: 'password') }.model
  }

  let(:user) { @user.reload }
end

shared_context "agent's sub accounts" do
  before(:all) {
    base_data = {password: '888888', password_confirmation: '888888'}
    @sub_shareholder = @shareholder.accounts.create(base_data.merge(nickname: 'sub_agent1', username: 'sub_agent1'))
    @sub_director = @director.accounts.create(base_data.merge(nickname: 'sub_agent2', username: 'sub_agent2'))
    @sub_agent = @agent.accounts.create(base_data.merge(nickname: 'sub_agent3', username: 'sub_agent3'))
  }

  let(:sub_shareholder) { @sub_shareholder.reload }
  let(:sub_director) { @sub_director.reload }
  let(:sub_agent) { @sub_agent.reload }
end

shared_context "agent signed in" do
  before { sign_in_as(agent) }
end
shared_context "director signed in" do
  before { sign_in_as(director) }
end
shared_context "shareholder signed in" do
  before { sign_in_as(shareholder) }
end
