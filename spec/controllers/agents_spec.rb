require "rails_helper"

describe AgentsController do
  include_context "site config"
  include_context "agent tree and machine"

  context "When sign in as master account" do
    pending
    # shared_examples "as as agent can view/edit lower level agent" do
    #
    #   before { sign_in login_agent }
    #
    #   context "When account enabled" do
    #     before { login_agent.update(lock: false) }
    #
    #     it "list should status 200" do
    #       get :index
    #       expect(response).to be_access_allow
    #     end
    #     it "show should status 200" do
    #       get :show, id: edit_agent.id
    #       expect(response).to be_access_allow
    #     end
    #     it "edit should status 200" do
    #       get :edit, id: edit_agent.id
    #       expect(response).to be_access_allow
    #     end
    #     it "update success should redirect to #show" do
    #       put :update, id: edit_agent.id, agent: {}
    #       expect(response).to redirect_to("/agents/#{edit_agent.id}")
    #     end
    #     it "update fail should re-render #edit" do
    #       put :update, id: edit_agent.id, agent: { password: '12345' }
    #       expect(response).to render_template(:edit)
    #     end
    #   end
    #
    #   context "When account locked" do
    #     before { login_agent.update(lock: true) }
    #
    #     it "list should status 200" do
    #       get :index
    #       expect(response).to be_access_allow
    #     end
    #     it "show should status 200" do
    #       get :show, id: edit_agent.id
    #       expect(response).to be_access_allow
    #     end
    #     it "edit should status 200" do
    #       get :edit, id: edit_agent.id
    #       expect(response).to be_access_denied(alert_message: '您的額度不足，目前僅能瀏覽，禁止任何操作。')
    #     end
    #     it "update success should redirect to #show" do
    #       put :update, id: edit_agent.id, agent: {}
    #       expect(response).to be_access_denied(alert_message: '您的額度不足，目前僅能瀏覽，禁止任何操作。')
    #     end
    #   end
    # end
    #
    # context "login as as shareholder" do
    #   it_behaves_like "as as agent can view/edit lower level agent" do
    #     let(:login_agent) { shareholder }
    #     let(:edit_agent) { director }
    #   end
    #
    #   it_behaves_like "as as agent can view/edit lower level agent" do
    #     let(:login_agent) { shareholder }
    #     let(:edit_agent) { agent }
    #   end
    # end
    #
    # context "login as as director" do
    #   it_behaves_like "as as agent can view/edit lower level agent" do
    #     let(:login_agent) { director }
    #     let(:edit_agent) { agent }
    #   end
    # end
  end

  context "When sign in as sub account" do
    # TODO: add test to sub account login
  end
end
