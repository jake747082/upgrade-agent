describe UsersController do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "user accounts"

  context "when cash site enabled" do
    include_context "cash site enabled"

    context "when agent signed in" do
      pending
      # before { sign_in agent }
      #
      # context "with enabled account" do
      #   before { agent.update(lock: false) }
      #   it_behaves_like "#index access allow"
      #   it_behaves_like "#show access allow" do
      #     let(:params) { { id: user.id } }
      #   end
      #
      #   it "#edit/update should access allow" do
      #     get :edit, id: user.id
      #     expect(response).to be_access_allow
      #     put :update, id: user.id, user: {}
      #     expect(response).to redirect_to "/users/#{user.id}"
      #   end
      # end
      #
      # context "with locked account" do
      #   before { agent.update(lock: true) }
      #   it_behaves_like "#index access allow"
      #   it_behaves_like "#show access allow" do
      #     let(:params) { { id: user.id } }
      #   end
      #
      #   it "#edit/update should access allow" do
      #     get :edit, id: user.id
      #     expect(response).to be_access_denied(alert_message: '您的額度不足，目前僅能瀏覽，禁止任何操作。')
      #     put :update, id: user.id, user: {}
      #     expect(response).to be_access_denied(alert_message: '您的額度不足，目前僅能瀏覽，禁止任何操作。')
      #   end
      # end
    end

    %w(shareholder director).each do |agent_level|
      context "when #{agent_level} signed in" do
        let(:login_agent) { send(agent_level) }
        before { sign_in login_agent }

        it_behaves_like "#index access allow"
        it_behaves_like "#show access allow" do
          let(:params) { { id: user.id } }
        end
      end
    end
  end

  context "when cash site disabled" do
    include_context "cash site disabled"
    # TODO: add when shop site disabled test
  end
end
