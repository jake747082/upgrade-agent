shared_examples "can view #index and #show" do
  it "#index should return 200" do
    get :index
    expect(response).to be_access_allow
  end

  it "#show with json should return 200" do
    get :show, id: machine.id, format: :js
    expect(response).to be_access_allow
  end
end

describe MachinesController do
  include_context "site config"
  include_context "agent tree and machine"

  context "when shop site enabled" do
    pending
    # include_context "shop site enabled"
    #
    # context "when agent signed in" do
    #   before { sign_in agent }
    #
    #   context "with enabled account" do
    #     before { agent.update(lock: false) }
    #     it_behaves_like "#index access allow"
    #     it_behaves_like "#show access allow" do
    #       let(:params) { { id: machine.id, format: :js } }
    #     end
    #
    #     it "#destroy with json should return 200" do
    #       allow_any_instance_of(MachineForm::Delete).to receive(:delete) { true }
    #       delete :destroy, id: machine.id, format: :js
    #       expect(response).to be_access_allow
    #     end
    #   end
    #
    #   context "with locked account" do
    #     before { agent.update(lock: true) }
    #     it_behaves_like "#index access allow"
    #     it_behaves_like "#show access allow" do
    #       let(:params) { { id: machine.id, format: :js } }
    #     end
    #
    #     it "#destroy with json should access denied" do
    #       allow_any_instance_of(MachineForm::Delete).to receive(:delete) { true }
    #       delete :destroy, id: machine.id, format: :js
    #       expect(response).to be_access_denied(alert_message: '您的額度不足，目前僅能瀏覽，禁止任何操作。')
    #     end
    #   end
    # end
    #
    # %w(shareholder director).each do |agent_level|
    #   context "when #{agent_level} signed in" do
    #     let(:login_agent) { send(agent_level) }
    #     before { sign_in login_agent }
    #
    #     it_behaves_like "#index access allow"
    #     it_behaves_like "#show access allow" do
    #       let(:params) { { id: machine.id, format: :js } }
    #     end
    #
    #     it "#destroy with json should return 403" do
    #       delete :destroy, id: machine.id, format: :js
    #       expect(response).to be_access_denied
    #     end
    #   end
    # end
  end

  context "when shop site disabled" do
    include_context "shop site disabled"
    # TODO: add when shop site disabled test
  end
end
