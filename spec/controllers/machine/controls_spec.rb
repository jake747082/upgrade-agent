require "rails_helper"

describe Machine::ControlsController do
  include_context "site config"
  include_context "agent tree and machine"

  context "when shop site enabled" do
    include_context "shop site enabled"
    before { sign_in agent }
    it "should boot machine" do
      machine.update!(boot: false)
      post :create, machine_id: machine.id, format: :js
      expect(response).to be_access_allow
      expect(machine.reload.boot).to eq true
    end
    it "should shutdown machine" do
      machine.update!(credit_max: 0, credit_used: 0, boot: true)
      delete :destroy, machine_id: machine.id, format: :js
      expect(response).to be_access_allow
      expect(machine.reload.boot).to eq false
    end
  end

  context "when shop site disabled" do
    include_context "shop site disabled"
    # TODO: add when shop site disabled test
  end
end
