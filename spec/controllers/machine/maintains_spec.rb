require "rails_helper"

describe Machine::MaintainsController do
  include_context "site config"
  include_context "agent tree and machine"

  context "when shop site enabled" do
    include_context "shop site enabled"
    before { sign_in agent }
    it "visit maintain page should status 200" do
      get :new
      expect(response).to be_access_allow
    end
    it "open maintain mode should status 200" do
      post :create
      expect(response).to be_access_allow
    end
    it "close maintain mode should status 200" do
      delete :destroy
      expect(response).to redirect_to('/maintains/new')
    end
  end
  context "when director signed in" do
    before { sign_in director }
    it "visit maintain page should status 302 redirect_to back or root_path with messages" do
      get :new
      expect(response).to be_access_denied
    end
    it "open maintain mode should status 404" do
      post :create
      expect(response).to be_access_denied
    end
    it "close maintain mode should status 404" do
      delete :destroy
      expect(response).to be_access_denied
    end
  end

  context "when shop site disabled" do
    include_context "shop site disabled"
    # TODO: add when shop site disabled test
  end
end