require "rails_helper"

describe Machine::DepositsController do
  include_context "site config"
  include_context "agent tree and machine"

  context "when shop site enabled" do
    include_context "shop site enabled"
    before { sign_in agent }
    it "visit deposit page should status 200" do
      post :new, machine_id: machine.id, format: :js
      expect(response).to be_access_allow
    end
    context "and deposit" do
      it "shutdown machine should deposit and boot" do
        machine.update!(boot: false, credit_max: 0)
        post :create, machine_id: machine.id, deposit: { deposit_credit: 100 }, format: :js
        expect(response).to be_access_allow
        expect(machine.reload.boot).to eq true
        expect(machine.reload.credit_max).to eq 100
      end
      it "boot machine should deposit" do
        machine.update!(boot: true, credit_max: 0)
        post :create, machine_id: machine.id, deposit: { deposit_credit: 100 }, format: :js
        expect(response).to be_access_allow
        expect(machine.reload.boot).to eq true
        expect(machine.reload.credit_max).to eq 100
      end
    end
  end

  context "when shop site disabled" do
    include_context "shop site disabled"
    # TODO: add when shop site disabled test
  end
end
