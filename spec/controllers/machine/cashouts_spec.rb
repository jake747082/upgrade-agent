require "rails_helper"

describe Machine::CashoutsController do
  include_context "site config"
  include_context "agent tree and machine"

  describe "when shop site enabled" do
    include_context "shop site enabled"
    before { sign_in agent }
    it "visit cashout page should status 200" do
      get :new, machine_id: machine.id, format: :js
      expect(response).to be_access_allow
    end
    it "visit withdraw page should status 200" do
      get :withdraw, machine_id: machine.id, format: :js
      expect(response).to be_access_allow
    end
    context "When cashout or withdraw" do
      before { machine.update!(credit_max: 100, credit_used: 0) }
      it "fail should status 200 and #credit_max, #credit_used not change" do
        post :create, machine_id: machine.id, format: :js
        expect(response).to be_access_allow
        expect(machine.credit_max).to eq(100)
        expect(machine.credit_used).to eq(0)
      end
      it "success should status 200 and #credit_max, #credit_used should reset to 0" do
        post :create, machine_id: machine.id, cashout: {cashout_credit: 100}, format: :js
        expect(response).to be_access_allow
        expect(machine.reload.credit_max).to eq 0
        expect(machine.reload.credit_used).to eq 0
      end
      it "when withdraw 90 credits success should status 200 and #credit_used should increment 90" do
        post :create, machine_id: machine.id, cashout: {cashout_credit: 90}, format: :js
        expect(response).to be_access_allow
        expect(machine.reload.credit_max).to eq 100
        expect(machine.reload.credit_used).to eq 90
      end
    end
  end

  describe "when shop site disabled" do
    include_context "shop site disabled"
    # TODO: add test when Setting.shop_site? == false
  end
end
