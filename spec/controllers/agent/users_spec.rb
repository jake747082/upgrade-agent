describe Agent::UsersController do
  include_context "site config"
  include_context "agent tree and machine"

  context "when cash site enabled" do
    include_context "cash site enabled"
    before {
      sign_in director
      allow_any_instance_of(Agent::UsersController).to receive(:users_path) { '' }
      allow_any_instance_of(Agent::UsersController).to receive(:users_url) { '' }
    }
    it "should success to visit agent/users list" do
      get :index, agent_id: agent.id
      expect(response).to be_access_allow
    end
  end

  context "when cash site disabled" do
    pending
    # include_context "cash site disabled"
    # before {
    #   sign_in director
    #   allow(Setting).to receive(:cash_site?) { false }
    #   allow_any_instance_of(Agent::UsersController).to receive(:users_path) { '' }
    #   allow_any_instance_of(Agent::UsersController).to receive(:users_url) { '' }
    # }
    # it "should faild to visit agent/users list" do
    #   get :index, agent_id: agent.id
    #   expect(response).to be_access_denied
    # end
  end
end
