require "rails_helper"

describe Agent::CashoutsController do
  include_context "site config"
  include_context "agent tree and machine"

  describe "[Controller] Agent Cashouts" do
    context "when director cashout agent" do
      before { sign_in director }
      it "success should redirect show page and show notice" do
        agent.update!(credit_used: 10)
        post :create, agent_id: agent.id
        expect(response).to redirect_to("/agents/#{agent.id}")
        expect(flash['notice']).to include '已成功結清帳款!'
      end
      it "fail should redirect show page" do
        allow_any_instance_of(Agent::Cashout).to receive(:call) { false }
        post :create, agent_id: agent.id
        expect(response).to redirect_to("/agents/#{agent.id}")
      end
    end
    it "agent should not allow cashout agent" do
      sign_in agent
      post :create, agent_id: agent.id
      expect(response).to redirect_to root_path
      expect(flash['alert']).to eq '您沒有存取該資源的權限。'
    end
    it "shareholder should not allow cashout agent" do
      sign_in shareholder
      post :create, agent_id: agent.id
      expect(response).to redirect_to root_path
      expect(flash['alert']).to eq '您沒有存取該資源的權限。'
    end
  end
end