require "rails_helper"

describe Agent::LevelsController do
  include_context "site config"
  include_context "agent tree and machine"

  describe "[Controller] Agent Levels" do
    context "director signed in" do
      before { sign_in director }
      it "visit agent levels list should status 200" do
        get :show, agent_level: agent.agent_level
        expect(response.code).to eq('200')
        expect(response).to render_template('agents/index')
      end
      it "visit agent levels list without agent level should status 404" do
        get :show
        expect(response.code).to eq('404')
      end
    end
    context "agent signed in" do
      before { sign_in agent }
      it "visit agent levels list should status 404" do
        get :show, agent_level: agent.agent_level
        expect(response).to redirect_to root_path
        expect(flash['alert']).to eq '您沒有存取該資源的權限。'
      end
    end
  end
end