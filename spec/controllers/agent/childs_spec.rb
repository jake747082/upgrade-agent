require "rails_helper"

describe Agent::ChildsController do
  include_context "site config"
  include_context "agent tree and machine"

  describe "[Controller] Agent Childs" do
    context "when director signed in" do
      before { sign_in director }
      it "list should status 200" do
        get :index, agent_id: director.id
        expect(response.code).to eq('200')
      end
      it "visit create page should status 200" do
        get :new, agent_id: director.id
        expect(response.code).to eq('200')
      end
      it "create child success should status 200" do
        allow_any_instance_of(AgentForm::Create).to receive(:create) { true }
        allow_any_instance_of(AgentForm::Create).to receive(:model) { director }
        post :create, agent_id: director.id, agent: {}
        expect(response).to redirect_to("/agents/#{director.id}")
      end
      it "create child fail should status 200" do
        post :create, agent_id: director.id, agent: {}
        expect(response).to render_template(:new)
      end
    end
    context "when agent signed in" do
      before { sign_in agent }
      it "list should status 404" do
        get :index, agent_id: agent.id
        expect(response).to redirect_to root_path
        expect(flash['alert']).to eq '您沒有存取該資源的權限。'
      end
      it "visit create page should status 404" do
        get :new, agent_id: agent.id
        expect(response).to redirect_to root_path
        expect(flash['alert']).to eq '您沒有存取該資源的權限。'
      end
      it "create child should status 404" do
        post :create, agent_id: agent.id, agent: {}
        expect(response).to redirect_to root_path
        expect(flash['alert']).to eq '您沒有存取該資源的權限。'
      end
    end
  end
end