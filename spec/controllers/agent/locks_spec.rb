require "rails_helper"

describe Agent::LocksController do
  include_context "site config"
  include_context "agent tree and machine"

  describe "[Controller] Agent Locks" do
    before { sign_in shareholder }

    context "When lock tree" do
      it "success should redirect edit page and show flash" do
        post :create, agent_id: director.id
        expect(response).to redirect_to("/agents/#{director.id}/edit")
        expect(flash['notice']).to include '已鎖定該帳戶和及其下線'
      end
      it "fail should redirect edit page" do
        allow_any_instance_of(Agent::TreeLock).to receive(:lock) { false }
        post :create, agent_id: director.id
        expect(response).to redirect_to("/agents/#{director.id}/edit")
      end
    end

    context "When unlock director" do
      it "success should redirect edit page and show flash" do
        delete :destroy, agent_id: director.id
        expect(response).to redirect_to("/agents/#{director.id}/edit")
        expect(flash['notice']).to include '已解鎖該帳戶'
      end

      it "fail should redirect edit page" do
        allow_any_instance_of(Agent::TreeLock).to receive(:unlock) { false }
        delete :destroy, agent_id: director.id
        expect(response).to redirect_to("/agents/#{director.id}/edit")
      end
    end

    context "When unlock director tree" do
      it "unlock tree success should redirect edit page and show flash" do
        delete :destroy, agent_id: director.id, tree: true
        expect(response).to redirect_to("/agents/#{director.id}/edit")
        expect(flash['notice']).to include '已解鎖該帳戶和及其下線'
      end

      it "unlock tree fail should redirect edit page" do
        allow_any_instance_of(Agent::TreeLock).to receive(:unlock) { false }
        delete :destroy, agent_id: director.id, tree: true
        expect(response).to redirect_to("/agents/#{director.id}/edit")
      end
    end
  end
end