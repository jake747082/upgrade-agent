describe "machine maintains flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  context "when shop site enabled" do
    include_context "shop site enabled"
    %w(shareholder director).each do |agent_level|
      context "when #{agent_level} signed in" do
        it "should permission denied maintain machines" do
          sign_in_as(send(agent_level))
          visit "/maintains/new"
          expect(page).to be_access_denied
        end
      end
    end

    context "when agent signed in" do
      pending
      # before { sign_in_as(@agent) }
      #
      # it "should allow visit maintain page" do
      #   visit "/maintains/new"
      #   expect(current_path).to eq "/maintains/new"
      #   expect(page).to be_access_allow
      # end
      #
      # it "open maintain success should get maintain code" do
      #   allow_any_instance_of(AgentForm::Auth).to receive(:verify) { true }
      #   visit "/maintains/new"
      #   expect(page).to be_access_allow
      #   click_button '確認'
      #   expect(page).to have_content '維護碼'
      #   expect(page).to be_access_allow
      # end
      #
      # it "open maintain fail should render new" do
      #   allow_any_instance_of(AgentForm::Auth).to receive(:verify) { false }
      #   visit "/maintains/new"
      #   expect(page).to be_access_allow
      #   click_button '確認'
      #   expect(page).to have_content '帳號'
      #   expect(page).to be_access_allow
      # end
      #
      # it "should permission denied for locked agent" do
      #   agent.update!(lock: true)
      #   visit "/maintains/new"
      #   expect(current_path).to_not eq "/maintains/new"
      #   expect(page).to have_content '您的額度不足，目前僅能瀏覽'
      # end
    end
  end

  context "when shop site disabled" do
    include_context "shop site disabled"
    before { sign_in_as(@agent) }

    context "when agent signed in" do
      it "should allow visit maintain page" do
        visit "/maintains/new"
        expect(page).to be_access_denied
      end

      it "open maintain success should get maintain code" do
        allow_any_instance_of(AgentForm::Auth).to receive(:verify) { true }
        visit "/maintains/new"
        expect(page).to be_access_denied
      end

      it "open maintain fail should render new" do
        allow_any_instance_of(AgentForm::Auth).to receive(:verify) { false }
        visit "/maintains/new"
        expect(page).to be_access_denied
      end

      it "should permission denied for locked agent" do
        agent.update!(lock: true)
        visit "/maintains/new"
        expect(page).to be_access_denied
      end
    end
  end
end
