describe "create child flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  %w(shareholder director).each do |agent_level|
    context "when #{agent_level} signed in" do
      pending
      # before {
      #   @test_agent = send(agent_level)
      #   sign_in_as(@test_agent)
      # }
      # it "allow visit create page" do
      #   visit "/agents/#{@test_agent.id}/childs/new"
      #   expect(current_path).to eq "/agents/#{@test_agent.id}/childs/new"
      #   expect(page.status_code).to eq 200
      # end
      #
      # it "create child success should flash success" do
      #   visit "/agents/#{@test_agent.id}/childs/new"
      #   within("#new_agent") do
      #     fill_in '暱稱', with: "#{agent_level}_test"
      #     fill_in '帳號', with: "#{agent_level}_test"
      #     fill_in '密碼', with: '888888'
      #     fill_in '密碼確認', with: '888888'
      #   end
      #   click_button '儲存'
      #   expect(page).to have_content '新增成功!'
      #   expect(page.status_code).to eq 200
      # end
      #
      # it "create child fail should render create page" do
      #   allow_any_instance_of(AgentForm::Create).to receive(:create) { false }
      #   visit "/agents/#{@test_agent.id}/childs/new"
      #   within("#new_agent") do
      #     fill_in '暱稱', with: "#{agent_level}_test2"
      #     fill_in '帳號', with: "#{agent_level}_test2"
      #     fill_in '密碼', with: '888888'
      #     fill_in '密碼確認', with: '888888'
      #   end
      #   click_button '儲存'
      #   expect(current_path).to eq "/agents/#{@test_agent.id}/childs"
      #   expect(page.status_code).to eq 200
      # end
      #
      # it "view agents list should permission denied when #{agent_level} locked" do
      #   @test_agent.update!(lock: true)
      #   visit "/agents"
      #   click_link '新增下線代理'
      #   expect(page).to be_access_denied(alert_message: '您的額度不足，目前僅能瀏覽，禁止任何操作')
      # end
    end
  end
  context "when agent signed in" do
    it "should permission denied visit create page" do
      sign_in_as(agent)
      visit "/agents/#{agent.id}/childs/new"
      expect(page).to be_access_denied
    end
  end
end
