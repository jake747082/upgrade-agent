describe "Agent's child page", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  context "when signed in as a shareholder" do
    before { sign_in_as shareholder }

    it "should allow view my childs" do
      visit "/agents/#{shareholder.id}/childs"
      expect(current_path).to eq "/agents/#{shareholder.id}/childs"
      expect(page.status_code).to eq 200
    end

    it "should permission denied" do
      visit "/agents/#{agent.id}/childs"
      expect(page).to_not be_access_denied
    end
  end

  context "when signed in as a director" do
    before { sign_in_as director }

    it "should allow view self childs" do
      visit "/agents/#{director.id}/childs"
      expect(page).to be_access_allow
    end

    it "should not found page when view parent childs" do
      visit "/agents/#{shareholder.id}/childs"
      expect(page).to be_render_404
    end
  end

  context "when signed in as a agent" do
    before { sign_in_as agent }

    it "should permission denied when view my agent" do
      visit "/agents/#{agent.id}/childs"
      expect(page).to be_access_denied
    end

    it "should permission denied when view parent childs" do
      visit "/agents/#{director.id}/childs"
      expect(page).to be_access_denied
    end
  end
end