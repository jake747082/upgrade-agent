describe "visit finances flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  %w(shareholder director agent).each do |agent_level|
    context "when #{agent_level} signed in" do
      it "should allow visit self finances" do
        test_agent = send(agent_level)
        sign_in_as(test_agent)
        visit "/agents/#{test_agent.id}/finances"
        expect(current_path).to eq "/agents/#{test_agent.id}/finances"
        expect(page.status_code).to eq 200
      end
    end
  end

  context "when agent signed in" do
    before { sign_in_as(@agent) }
    it "view director finances should permission denied" do
      visit "/agents/#{director.id}/finances"
      expect(page).to be_render_404
    end

    it "view shareholder finances should permission denied" do
      visit "/agents/#{shareholder.id}/finances"
      expect(current_path).to eq "/agents/#{shareholder.id}/finances"
      expect(page).to be_render_404
    end
  end

  context "when director signed in" do
    before { sign_in_as(@director) }
    it "view agent finances should allow" do
      visit "/agents/#{agent.id}/finances"
      expect(current_path).to eq "/agents/#{agent.id}/finances"
      expect(page.status_code).to eq 200
    end

    it "view shareholder finances should permission denied" do
      visit "/agents/#{shareholder.id}/finances"
      expect(page).to be_render_404
    end
  end

  context "when shareholder signed in" do
    before { sign_in_as(@shareholder) }
    it "view agent finances should allow" do
      visit "/agents/#{agent.id}/finances"
      expect(current_path).to eq "/agents/#{agent.id}/finances"
      expect(page.status_code).to eq 200
    end

    it "view director finances should allow" do
      visit "/agents/#{director.id}/finances"
      expect(current_path).to eq "/agents/#{director.id}/finances"
      expect(page.status_code).to eq 200
    end
  end
end