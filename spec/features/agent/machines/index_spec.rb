describe "visit machines list flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  context "when shop site enabled" do
    include_context "shop site enabled"
    %w(shareholder director agent).each do |agent_level|
      context "when #{agent_level} signed in" do
        it "view self machines list should allow" do
          login_agent = send(agent_level)
          sign_in_as(login_agent)
          visit "/agents/#{login_agent.id}/machines"
          expect(current_path).to eq "/agents/#{login_agent.id}/machines"
          expect(page.status_code).to eq 200
        end
      end
    end

    context "when agent signed in" do
      before { sign_in_as(@agent) }

      it "view director machines should permission denied" do
        visit "/agents/#{director.id}/machines"
        expect(page).to be_render_404
      end
      it "view shareholder machines should permission denied" do
        visit "/agents/#{shareholder.id}/machines"
        expect(page).to be_render_404
      end
    end

    context "when director signed in" do
      before { sign_in_as(@director) }

      it "view agent machines should allow" do
        visit "/agents/#{agent.id}/machines"
        expect(current_path).to eq "/agents/#{agent.id}/machines"
        expect(page.status_code).to eq 200
      end
      it "view shareholder machines should permission denied" do
        visit "/agents/#{shareholder.id}/machines"
        expect(page).to be_render_404
      end
    end

    context "when shareholder signed in" do
      before { sign_in_as(@shareholder) }

      it "view agent machines should allow" do
        visit "/agents/#{agent.id}/machines"
        expect(current_path).to eq "/agents/#{agent.id}/machines"
        expect(page.status_code).to eq 200
      end
      it "view director machines should allow" do
        visit "/agents/#{director.id}/machines"
        expect(current_path).to eq "/agents/#{director.id}/machines"
        expect(page.status_code).to eq 200
      end
    end
  end

  context "when shop site disabled" do
    include_context "shop site disabled"
    %w(shareholder director agent).each do |agent_level|
      context "when #{agent_level} signed in" do
        before { 
          @login_agent = send(agent_level)
          sign_in_as(@login_agent)
        }
        it "view self machines list should allow" do
          visit "/agents/#{@login_agent.id}/machines"
          expect(page).to be_access_denied
        end

        it "can not visit childs machines" do
          @login_agent.ancestors.each do |child_agent|
            visit "/agents/#{child_agent.id}/machines"
            expect(page).to be_access_denied 
          end
        end
      end
    end
  end
end