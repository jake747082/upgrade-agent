describe "visit level agents list flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  context "when shareholder signed in" do
    before { sign_in_as(@shareholder) }
    it "view shareholder list should allow" do
      visit "/agent/levels/?agent_level=shareholder"
      expect(page).to be_render_404
    end
    %w(director agent).each do |agent_level|
      it "view #{agent_level} list should allow" do
        visit "/agent/levels/?agent_level=#{agent_level}"
        expect(current_path).to eq "/agent/levels/"
        expect(page.status_code).to eq 200
      end
    end
  end

  context "when director signed in" do
    before { sign_in_as(@director) }
    it "view agent list should allow" do
      visit "/agent/levels/?agent_level=agent"
      expect(current_path).to eq "/agent/levels/"
      expect(page.status_code).to eq 200
    end
    %w(shareholder director).each do |agent_level|
      it "view #{agent_level} list should permission denied" do
        visit "/agent/levels/?agent_level=#{agent_level}"
        expect(page).to be_render_404
      end
    end
  end

  context "when agent signed in" do
    before { sign_in_as(@agent) }
    %w(shareholder director agent).each do |agent_level|
      it "view #{agent_level} list should permission denied" do
        visit "/agent/levels/?agent_level=#{agent_level}"
        expect(page).to be_access_denied
      end
    end
  end
end