describe "visit change logs flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  %w(shareholder director agent).each do |agent_level|
    context "when #{agent_level} signed in" do
      it "view self change logs should allow" do
        test_agent = send(agent_level)
        sign_in_as(test_agent)
        visit "/agents/#{test_agent.id}/logs/change"
        expect(current_path).to eq "/agents/#{test_agent.id}/logs/change"
        expect(page.status_code).to eq 200
      end
    end
  end

  context "when agent signed in" do
    before { sign_in_as(@agent) }
    it "view director change logs should permission denied" do
      visit "/agents/#{director.id}/logs/change"
      expect(page).to be_render_404
    end
    it "view shareholder change logs should permission denied" do
      visit "/agents/#{shareholder.id}/logs/change"
      expect(page).to be_render_404
    end
  end

  context "when director signed in" do
    before { sign_in_as(@director) }
    it "view agent change logs should allow" do
      visit "/agents/#{agent.id}/logs/change"
      expect(current_path).to eq "/agents/#{agent.id}/logs/change"
      expect(page.status_code).to eq 200
    end
    it "view shareholder change logs should permission denied" do
      visit "/agents/#{shareholder.id}/logs/change"
      expect(current_path).to eq "/agents/#{shareholder.id}/logs/change"
      expect(page).to be_render_404
    end
  end

  context "when shareholder signed in" do
    before { sign_in_as(@shareholder) }
    it "view agent change logs should allow" do
      visit "/agents/#{agent.id}/logs/change"
      expect(current_path).to eq "/agents/#{agent.id}/logs/change"
      expect(page.status_code).to eq 200
    end
    it "view director change logs should allow" do
      visit "/agents/#{director.id}/logs/change"
      expect(current_path).to eq "/agents/#{director.id}/logs/change"
      expect(page.status_code).to eq 200
    end
  end
end