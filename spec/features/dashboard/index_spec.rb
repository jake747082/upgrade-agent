describe "visit dashboard url: '/'", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"
  include_context "agent's sub accounts"


  shared_examples "as a agent can visit AGENT dashboard" do
    before { sign_in_as(login_agent) }

    it "can visit / dashboard" do
      visit '/'
      expect(page).to be_access_allow
      expect(page).to_not have_css('#shop-dashboard')
      expect(page).to have_content '控制台'
    end
  end

  shared_examples "as a agent can visit SHOP dashboard" do
    before { sign_in_as(login_agent) }

    it "can visit / dashboard" do
      visit '/'
      expect(page).to be_access_allow
      expect(page).to have_css('#shop-dashboard')
      expect(page).to have_css('#boot-machines-panel')
      expect(page).to have_css('#shutdown-machines-panel')
      expect(page).to have_css('#machine-logs-panel')
      expect(page).to have_css 'a[href="/maintains/new"]'
    end
  end

  context "when shareholder/director signin" do
    context "shareholder" do
      it_behaves_like "as a agent can visit AGENT dashboard" do
        let(:login_agent) { shareholder }
      end
    end

    context "director" do
      it_behaves_like "as a agent can visit AGENT dashboard" do
        let(:login_agent) { director }
      end
    end
  end

  context "when shop site enabled" do
    include_context "shop site enabled"
    it_behaves_like "as a agent can visit SHOP dashboard" do
      let(:login_agent) { agent }
    end
  end

  context "when shop site disabled" do
    include_context "shop site disabled"
    it_behaves_like "as a agent can visit AGENT dashboard" do
      let(:login_agent) { agent }
    end
  end
end