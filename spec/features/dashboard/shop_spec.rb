describe "visit shop dashboard flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"
  include_context "agent's sub accounts"

  context "when shop site enabled" do
    include_context "shop site enabled"
    
    shared_examples "a shop dashboard" do
      before {
        agent.update(credit_max: 20000, credit_used: 0, casino_ratio: 50)
        machine.update(credit_max: 0, credit_used: 0, boot: false)
      }

      it "agent view dashboard" do
        visit '/'
        expect(current_path).to eq '/'
        expect(page.status_code).to eq 200
        expect(page).to have_content '開檯'
        expect(page).to have_content '關檯'
        expect(page).to have_content '記錄'
      end

      it "agent can see self information and 1 shutdown machine, and 0 boot machine" do
        visit '/'

        expect(find('#credit_left').text).to eq '20,000.00'
        expect(find('#credit_max').text).to eq '20,000.00'
        expect(find('#casino_ratio').text).to eq '50.0%'

        machine_row = find("#machine_#{machine.id} .credit-left")
        expect(machine_row.text).to eq '0.00'

        expect(page).to_not have_css("#boot-machines-panel .machine")
        expect(page).to have_css("#shutdown-machines-panel .machine", count: 1)
      end

      it "agent view dashboard with open maintain mode" do
        agent.update!(maintain_code: '12345')
        visit '/'
        expect(current_path).to eq '/'
        expect(page.status_code).to eq 200
        expect(page).to have_content '開檯'
        expect(page).to have_content '關檯'
        expect(page).to have_content '記錄'
        expect(page).to have_content '維護中'
      end

      it "agent view dashboard with close maintain mode" do
        agent.update!(maintain_code: nil)
        visit '/'
        expect(current_path).to eq '/'
        expect(page.status_code).to eq 200
        expect(page).to have_content '開檯'
        expect(page).to have_content '關檯'
        expect(page).to have_content '記錄'
        expect(page).to have_content '維護模式'
      end
    end

    context "with signed_in master account" do
      include_context "agent signed in"
      it_behaves_like "a shop dashboard"

      let(:machine_row) { find("#machine_#{machine.id}") }
      let(:machine_credit) { machine_row.find(".credit-left") }
      let(:machine_logs) { all('tr.public_activity_activity') }
      let(:machine_boot) { find('#boot-machines-panel') }
      let(:machine_shutdown) { find('#shutdown-machines-panel') }

      it "can cashout a shutdown machines", js: true do
        visit '/'

        expect(machine_row).to have_css('.btn', text: '結帳', visible: true)

        click_link "結帳", match: :first

        expect(page).to have_content "機檯剩餘金額"

        click_button "結帳", match: :first

        wait_for_ajax_then do
          expect(machine_credit.text).to eq '0.00'
          expect(machine_logs.count).to eq 1
        end
      end

      it "can trun depoist a machine and it will turn on", js: true do
        visit '/'
        within("#machine_#{machine.id}") do
          click_link '+'
        end

        within("#deposit") do
          fill_in "儲值現金籌碼", with: '100'
        end
        click_button '確認'

        wait_for_ajax_then do
          expect(machine_credit.text).to eq '100.00'
          expect(machine_logs.count).to eq 2
        end
      end

      it "can turn casout a booting machine and it will turn off", js: true do
        visit '/'

        within("#machine_#{machine.id}") do
          find('a.shutdown').trigger('click')
        end

        wait_for_ajax_then do
          expect(machine_boot.all('.machine').count).to eq 0
          expect(machine_shutdown.all('.machine').count).to eq 1
        end

        click_button "結帳", match: :first

        wait_for_ajax_then do
          expect(machine_credit.text).to eq '0.00'
          expect(machine_logs.count).to eq 3
        end
      end
    end

    context "with signed_in sub account" do
      before { sign_in_as(sub_agent) }
      it_behaves_like "a shop dashboard"

      it "can NOT control machines (deposit, on/off, cashout ...)", js: true do
        visit '/'
        allow(machine).to receive(:can_deposit?) { true }
        visit '/'
        expect(page).to_not have_css('.btn', text: '結帳')
        expect(page).to_not have_css('.btn', text: '+')
        allow(machine).to receive(:credit_left) { 0 }
        expect(page).to_not have_css('.btn', text: '-')
      end

      it "can NOT control machines (deposit, on/off, cashout ...)", js: true do
        visit '/'
        allow(machine).to receive(:can_deposit?) { true }
        visit '/'
        expect(page).to_not have_css('.btn', text: '結帳')
        expect(page).to_not have_css('.btn', text: '+')
        allow(machine).to receive(:credit_left) { 0 }
        expect(page).to_not have_css('.btn', text: '-')
      end
    end

  end
end