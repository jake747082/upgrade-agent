describe "visit machines list flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  context "when shop site enabled" do
    include_context "shop site enabled"
    %w(shareholder director agent).each do |agent_level|
      it "#{agent_level} should allow visit machines page" do
        sign_in_as(send(agent_level))
        visit '/machines'
        expect(page).to be_access_allow
      end
    end
  end

  context "when shop site disabled" do
    include_context "shop site disabled"
    %w(shareholder director agent).each do |agent_level|
      it "#{agent_level} should allow visit machines page" do
        sign_in_as(send(agent_level))
        visit '/machines'
        expect(page).to be_access_denied
      end
    end
  end
end