describe "edit current account flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"
  include_context "agent's sub accounts"

  %w(shareholder director agent).each do |agent_level|
    context "when #{agent_level} signed in" do
      before { sign_in_as(send(agent_level)) }

      it "should allow visit edit page" do
        visit '/my/edit'
        expect(current_path).to eq '/my/edit'
        expect(page.status_code).to eq 200
      end

      it "current password not match should fail" do
        visit '/my/edit'
        within("form") do
          fill_in 'agent_password', with: '777777'
          fill_in 'agent_password_confirmation', with: '777777'
          fill_in 'agent_current_password', with: '999999'
        end
        click_button '儲存'
        expect(current_path).to eq '/my'
        expect(page).to have_content '是無效的'
        expect(page).to have_css('.has-error #agent_current_password')
      end

      it "edit but don't change passowrd should success" do
        visit '/my/edit'
        within("form") do
          fill_in 'agent_password', with: ''
          fill_in 'agent_password_confirmation', with: ''
          fill_in 'agent_current_password', with: '888888'
        end
        click_button '儲存'
        expect(current_path).to eq '/'
        expect(page).to have_content '您已經成功的更新帳號資訊。'
      end

      it "change password should success" do
        visit '/my/edit'
        within("form") do
          fill_in 'agent_password', with: '999999'
          fill_in 'agent_password_confirmation', with: '999999'
          fill_in 'agent_current_password', with: '888888'
        end
        click_button '儲存'
        expect(current_path).to eq '/'
        expect(page).to have_content '您已經成功的更新帳號資訊。'
      end
    end

    context "when #{agent_level}'s sub account signed in" do

      before { sign_in_as(send("sub_#{agent_level}")) }

      it "should allow visit edit page" do
        visit '/sub_account/edit'
        expect(current_path).to eq '/sub_account/edit'
        expect(page.status_code).to eq 200
      end

      it "current password not match should fail" do
        visit '/sub_account/edit'
        within("form") do
          fill_in 'agent_sub_account_password', with: '777777'
          fill_in 'agent_sub_account_password_confirmation', with: '777777'
          fill_in 'agent_sub_account_current_password', with: '999999'
        end
        click_button '儲存'
        expect(current_path).to eq '/sub_account'
        expect(page).to have_content '是無效的'
        expect(page).to have_css('.has-error #agent_sub_account_current_password')
      end

      it "edit but don't change passowrd should success" do
        visit '/sub_account/edit'
        within("form") do
          fill_in 'agent_sub_account_password', with: ''
          fill_in 'agent_sub_account_password_confirmation', with: ''
          fill_in 'agent_sub_account_current_password', with: '888888'
        end
        click_button '儲存'
        expect(current_path).to eq '/'
        expect(page).to have_content '您已經成功的更新帳號資訊。'
      end

      it "change password should success" do
        visit '/sub_account/edit'
        within("form") do
          fill_in 'agent_sub_account_password', with: '999999'
          fill_in 'agent_sub_account_password_confirmation', with: '999999'
          fill_in 'agent_sub_account_current_password', with: '888888'
        end
        click_button '儲存'
        expect(current_path).to eq '/'
        expect(page).to have_content '您已經成功的更新帳號資訊。'
      end
    end
  end
end