describe "login flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"
  include_context "agent's sub accounts"

  it "visit master account login page" do
    visit '/my/sign_in'
    expect(current_path).to eq '/my/sign_in'
    expect(page.status_code).to eq 200
  end

  it "visit sub account login page" do
    visit '/sub_account/sign_in'
    expect(current_path).to eq '/sub_account/sign_in'
    expect(page.status_code).to eq 200
  end

  %w(shareholder director agent).each do |agent_level|
    it "sign in as #{agent_level}" do
      login_agent = send(agent_level)
      sign_in_as login_agent
      expect(page).to have_content '成功登入了。'
      expect(page).to have_content login_agent.human_agent_level
      expect(page).to have_content login_agent.username
      expect(current_path).to eq '/'
    end

    it "sign in as #{agent_level}' sub account" do
      login_sub_account = send("sub_#{agent_level}")
      sign_in_as(login_sub_account)
      expect(page).to have_content '成功登入了。'
      expect(page).to have_content login_sub_account.human_agent_level
      expect(page).to have_content login_sub_account.username
      expect(current_path).to eq '/'
    end
  end
end