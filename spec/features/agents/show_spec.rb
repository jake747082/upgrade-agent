describe "visit agent flow", type: :feature do
  include_context "site config"
  include_context "slot game"
  include_context "agent tree and machine"

  %w(shareholder director).each do |agent_level|
    context "when #{agent_level} signed in" do
      pending
      # before {
      #   @test_agent = send(agent_level)
      #   sign_in_as(@test_agent)
      # }
      # it "edit self should permission denied" do
      #   visit "/agents/#{@test_agent.id}"
      #   expect(current_path).to eq "/agents/#{@test_agent.id}"
      #   expect(page).to be_render_404
      # end
      #
      # it "edit agent should allow" do
      #   visit "/agents/#{agent.id}"
      #   expect(current_path).to eq "/agents/#{agent.id}"
      #   expect(page.status_code).to eq 200
      # end
      #
      # it "should permission denied for locked #{agent_level}" do
      #   @test_agent.update!(lock: true)
      #   visit "/agents/#{agent.id}/edit"
      #   expect(page).to be_access_denied(alert_message: '您的額度不足，目前僅能瀏覽，禁止任何操作')
      # end
    end
  end

  context "when agent signed in" do
    before { sign_in_as(@agent) }

    it "view self should permission denied" do
      visit "/agents/#{agent.id}"
      expect(page).to be_access_denied
    end
    it "view director should permission denied" do
      visit "/agents/#{director.id}"
      expect(page).to be_access_denied
    end
    it "view shareholder should permission denied" do
      visit "/agents/#{shareholder.id}"
      expect(page).to be_access_denied
    end
  end

  context "when director signed in" do
    it "view shareholder should permission denied" do
      sign_in_as(director)
      visit "/agents/#{shareholder.id}"
      expect(page).to be_render_404
    end
  end

  context "when shareholder signed in" do
    it "view director should permission denied" do
      sign_in_as(shareholder)
      visit "/agents/#{director.id}"
      expect(current_path).to eq "/agents/#{director.id}"
      expect(page.status_code).to eq 200
    end
  end
end
